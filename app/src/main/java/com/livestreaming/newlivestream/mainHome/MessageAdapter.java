package com.livestreaming.newlivestream.mainHome;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.livestreaming.newlivestream.R;
import com.livestreaming.newlivestream.ResponcePojo.Messages;

import java.util.ArrayList;

/**
 * Created by Ali on 12/24/2018.
 */
public class MessageAdapter extends RecyclerView.Adapter {

    private ArrayList<Messages> areMessage;
    private Context mContext;

    public MessageAdapter(ArrayList<Messages> areMessage, Context mContext) {
        this.areMessage = areMessage;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(mContext).inflate(R.layout.message_adapter,viewGroup,false);
        return new ViewMessageHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        ViewMessageHolder holder = (ViewMessageHolder) viewHolder;
      //  holder.tvMessage.setText(areMessage.get(i).getMessage());

        String name = areMessage.get(i).getUserName()+": ";
        String quant = name+areMessage.get(i).getMessage();
        SpannableString ss1=  new SpannableString(quant);
        ss1.setSpan(new RelativeSizeSpan(1.1f), 0,name.length()-1, 0);
        holder.tvMessage.setText(ss1);
    }

    @Override
    public int getItemCount() {
        return areMessage.size();
    }

    private class ViewMessageHolder extends RecyclerView.ViewHolder
    {
        private TextView tvMessage;
        private ViewMessageHolder(@NonNull View itemView) {
            super(itemView);
            tvMessage = itemView.findViewById(R.id.tvMessage);

        }
    }
}
