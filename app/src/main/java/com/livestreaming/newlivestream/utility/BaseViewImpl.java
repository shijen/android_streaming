package com.livestreaming.newlivestream.utility;

/**
 * Created by Ali on 11/21/2018.
 */
public interface BaseViewImpl
{
    void showProgress();
    void hideProgress();
    void apiError(String errorMsg);
    void onNetworkRetry(boolean isNetwork, String thisApi);

}
