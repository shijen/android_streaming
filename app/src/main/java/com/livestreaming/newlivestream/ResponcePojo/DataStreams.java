package com.livestreaming.newlivestream.ResponcePojo;

import java.io.Serializable;

/**
 * Created by Ali on 12/19/2018.
 */
public class DataStreams implements Serializable
{
    /*"id":"5c1796e2907f1a5e39471b7f",
"name":"rtmpstream-1545049826518",
"userId":"5c04f70f34ea6318eb6c420a",
"action":"start",
"userName":"mahi",
"userImage":"",
"thumbnail":"/storage/emulated/0/YourDirectName/1545049826535.jpg",
"started":1545049826,
"viewers":0,
"allViewers":0*/

    private String streamId,id,name,userId,action,userName,userImage,thumbnail,started,viewers,allViewers;


    public String getStreamId() {
        return streamId;
    }

    public void setStreamId(String streamId) {
        this.streamId = streamId;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUserId() {
        return userId;
    }

    public String getAction() {
        return action;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserImage() {
        return userImage;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getStarted() {
        return started;
    }

    public String getViewers() {
        return viewers;
    }

    public String getAllViewers() {
        return allViewers;
    }
}
