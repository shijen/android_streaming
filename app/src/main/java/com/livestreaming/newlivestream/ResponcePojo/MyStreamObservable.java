package com.livestreaming.newlivestream.ResponcePojo;

import android.util.Log;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.Observer;

/**
 * Created by Ali on 12/19/2018.
 */
public class MyStreamObservable extends Observable<DataStreams> {

    private Observer<? super DataStreams> observer;
    private static MyStreamObservable observebleClass;
    private ArrayList<Observer<? super DataStreams>> myListObserver = new ArrayList<>();

    public static MyStreamObservable getInstance() {
        if (observebleClass == null) {
            observebleClass = new MyStreamObservable();

            return observebleClass;
        } else {
            return observebleClass;
        }
    }

    @Override
    protected void subscribeActual(Observer<? super DataStreams> observer) {
      this.observer = observer;
        Log.w("TAG", "emitsubscribeActual: "+myListObserver.size());
        if(!myListObserver.contains(observer))
        {
            myListObserver.add(observer);
            Log.w("TAG", "emitsubscribeActual1: "+myListObserver.size());
        }
    }

    public void removeObserver(Observer<? super DataStreams> observer)
    {
        Log.w("TAG", "emitremoveObserver: "+myListObserver.size());
        if(myListObserver.contains(observer))
        {
            myListObserver.remove(observer);
            Log.w("TAG", "emitremoveObserver1: "+myListObserver.size());
        }

    }

    public void emitJobStatus(DataStreams myBookingStatus)
    {
        Log.d("TAG", "emitJobStatus: "+myBookingStatus.getAction());
      //  observer.onNext(myBookingStatus);
      //  observer.onComplete();



        Log.w("TAG", "emit: "+myListObserver.size());
        for (int i =0 ;i<myListObserver.size();i++)
        {
            Observer<?super DataStreams> tempobserver = myListObserver.get(i);
            tempobserver.onNext(myBookingStatus);
            tempobserver.onComplete();
        }

    }
}
