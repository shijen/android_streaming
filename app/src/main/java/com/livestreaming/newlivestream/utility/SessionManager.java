package com.livestreaming.newlivestream.utility;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;

/**
 * <h>SessionManager</h>
 * Created by ${3Embed} on 6/11/17.
 */

public class SessionManager implements SessionManagerImpl{

    private static final String PREF_NAME = "localServiceProvider";
    // Constructor

    private final String SID = "SID";
    private final String AUTH = "TOKEN";
    private SharedPreferences pref;
    // Editor for Shared preferences
    private SharedPreferences.Editor editor;
    // Context
    Context context;
    // Shared pref mode
    private final int PRIVATE_MODE = 0;
    private String countryCode;
    private static SessionManager helper;

    @Inject
    public SessionManager(Context context)
    {
        this.context = context;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        editor.apply();
    }

    public static SessionManager getInstance(Context context){
        if(helper==null){
            helper=new SessionManager(context);
        }
        return helper;
    }

    @Override
    public String getSID() {
        return pref.getString(SID,"");
    }
    @Override
    public void setSID(String sid) {
        editor.putString(SID,sid);
        editor.commit();
    }
    @Override
    public void clearSession() {
        editor.clear();
        editor.commit();
    }

    @Override
    public void setHomeScreenData(String Data) {
        editor.putString("HomeData",Data);
        editor.commit();
    }

    @Override
    public String getHomeScreenData() {
         return pref.getString("HomeData","");
    }

    @Override
    public String getAUTH() {
        return pref.getString(AUTH,"");
    }

    @Override
    public void setAUTH(String auth) {
        editor.putString(AUTH,auth);
        editor.commit();
    }
    @Override
    public void setLatitude(String latitude)
    {
        editor.putString("latitude",latitude);
        editor.commit();
    }
    @Override
    public String getLatitude()
    {
        return pref.getString("latitude","");
    }
    /*******************************************************/
    @Override
    public void setLongitude(String longitude)
    {
        editor.putString("longitude",longitude);
        editor.commit();
    }
    @Override
    public String getLongitude()
    {
        return pref.getString("longitude","");
    }


    @Override
    public void setAddress(String address) {
        editor.putString("Address",address);
        editor.commit();
    }

    @Override
    public String getAddress() {
        return  pref.getString("Address","");
    }

    @Override
    public void setFirstName(String firstName) {

        editor.putString("FirstName",firstName);
        editor.commit();
    }

    @Override
    public String getFirstName() {
        return pref.getString("FirstName","");
    }

    @Override
    public void setLastName(String lastName) {
        editor.putString("LastName",lastName);
        editor.commit();
    }

    @Override
    public String getLastName() {
        return pref.getString("LastName","");
    }

    @Override
    public void setEmail(String email) {

        editor.putString("Email",email);
        editor.commit();
    }

    @Override
    public String getEmail() {
        return pref.getString("Email","");
    }

    @Override
    public void setMobileNo(String mobileNo) {

        editor.putString("MobileNo",mobileNo);
        editor.commit();
    }

    @Override
    public String getMobileNo() {
        return pref.getString("MobileNo","");
    }

    @Override
    public void setAbout(String about)
    {
        editor.putString("AboutMe",about);
        editor.commit();
    }

    @Override
    public String getAbout()
    {
        return pref.getString("AboutMe","");
    }

    @Override
    public void setRegisterId(String registerId)
    {
        editor.putString("RegisterId",registerId);
        editor.commit();
    }

    @Override
    public String getRegisterId() {
        return pref.getString("RegisterId","");
    }

    @Override
    public void setReferralCode(String refCode) {
        editor.putString("refRalCode",refCode);
        editor.commit();
    }

    @Override
    public String getReferralCode() {
        return pref.getString("refRalCode","");
    }

    @Override
    public void setGuestLogin(boolean guestLogin) {

        editor.putBoolean("guestLogin",guestLogin);
        editor.commit();
    }

    @Override
    public boolean getGuestLogin() {
        return pref.getBoolean("guestLogin",false);
    }

    @Override
    public void setProfileCalled(boolean isProfileCalled)
    {
        editor.putBoolean("IsProfileCalled",isProfileCalled);
        editor.commit();
    }

    @Override
    public boolean isProfileCalled() {
        return pref.getBoolean("IsProfileCalled",false);
    }


    @Override
    public String getProfilePicUrl() {
        return pref.getString("ProfilePic","");
    }

    @Override
    public void setProfilePicUrl(String profilePicUrl) {

        editor.putString("ProfilePic",profilePicUrl);
        editor.commit();
    }

    @Override
    public String getCountryCode() {
        return pref.getString("CountryCode","");
    }


    public void setCountryCode(String countryCode)
    {
        editor.putString("CountryCode",countryCode);
        editor.commit();
    }


    @Override
    public String getFcmTopic() {
        return pref.getString("FcmTopic", "");
    }

    @Override
    public void setFcmTopic(String fcmTopic) {
        editor.putString("FcmTopic", fcmTopic);
        editor.commit();
    }





}
