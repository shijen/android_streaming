package com.livestreaming.newlivestream.mainHome;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.livestreaming.newlivestream.MyApplication;
import com.livestreaming.newlivestream.R;
import com.livestreaming.newlivestream.ResponcePojo.DataStreams;
import com.livestreaming.newlivestream.ResponcePojo.MyStreamObservable;
import com.livestreaming.newlivestream.liveVideoPlayerStreamsLive.liveVideoPlayer.LiveVideoPlayer;
import com.livestreaming.newlivestream.mqtt.MQTTManager;
import com.livestreaming.newlivestream.mqtt.MqttEvents;
import com.livestreaming.newlivestream.network.ConnectivityReceiver;
import com.livestreaming.newlivestream.utility.AppTypeface;
import com.livestreaming.newlivestream.utility.SessionManagerImpl;
import com.livestreaming.newlivestream.utility.Utility;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerAppCompatActivity;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by Ali on 11/20/2018.
 */
public class MainActivity extends DaggerAppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener,MainActivityPresenterImpl.ViewMain
{
    @Inject MainActivityPresenterImpl.presenterMain presenterMain;

    @BindView(R.id.tvVideoBroadCaster)
    ImageView tvVideoBroadCaster;
    @BindView(R.id.tvNoBroadCaster)
    TextView tvNoBroadCaster;
    @BindView(R.id.onLiveRecyclerView)
    RecyclerView onLiveRecyclerView;
    GridLayoutManager gridLayoutManager;
    private ArrayList<DataStreams> dataStreams = new ArrayList<>();
    private AdapterMainLiveBroadcaster adapterMainLiveBroadcaster;


    @Inject
    AppTypeface appTypeface;
    @Inject
    MQTTManager mqttManager;
    @Inject
    SessionManagerImpl manager;

    public static final String RTMP_BASE_URL = "rtmp://karry.xyz:1935/live/";
   // public static final String RTMP_BASE_URL = "rtmp://139.59.124.59:1935/live/";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        registerReceiver(new ConnectivityReceiver(),
                new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        typeFace();
        callApi();
        createMqttConnection();
        rxObServable();
        Log.d("TAG", "onCreate: "+ Utility.currentTIme());

    }

    private void rxObServable() {

        Observer<DataStreams> observer = new Observer<DataStreams>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(DataStreams dataStream) {

                Log.d("TAG", "onNextRXOBSERVER: "+dataStream.getAction());
                if(dataStream!=null)
                {
                    if(dataStream.getAction().equals("start"))
                    {
                        dataStreams.add(dataStream);
                        tvNoBroadCaster.setVisibility(View.GONE);
                        onLiveRecyclerView.setVisibility(View.VISIBLE);
                        adapterMainLiveBroadcaster.notifyDataSetChanged();

                    }else if(dataStream.getAction().equalsIgnoreCase("stop"))
                    {
                        if(dataStreams.size()>0)
                        {
                            for(int i = 0; i<dataStreams.size();i++)
                            {
                                if(dataStream.getStreamId().equals(dataStreams.get(i).getId()))
                                {
                                    dataStreams.remove(dataStreams.get(i));
                                    adapterMainLiveBroadcaster.notifyDataSetChanged();
                                    if(dataStreams.size()>0)
                                    {
                                        tvNoBroadCaster.setVisibility(View.GONE);
                                        onLiveRecyclerView.setVisibility(View.VISIBLE);
                                    }else
                                    {
                                        tvNoBroadCaster.setVisibility(View.VISIBLE);
                                        onLiveRecyclerView.setVisibility(View.GONE);
                                    }
                                }
                            }
                        }
                    }
                }

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
        MyStreamObservable.getInstance().subscribe(observer);


     //   MyStreamObservable.getInstance().subscribe(observer);
    }

    private void createMqttConnection() {
        if(!mqttManager.isMQTTConnected())
        {
            mqttManager.createMQttConnection(manager.getSID());

            new Handler().postDelayed(() -> {

                if(mqttManager.isMQTTConnected())
                    mqttManager.subscribeToTopic(MqttEvents.StreamNow.value + "/" , 1);
            },1000);
        }else
        {
            mqttManager.subscribeToTopic(MqttEvents.StreamNow.value + "/" , 1);
        }
    }

    private void callApi() {
        presenterMain.callLiveBroadcaster();
    }

    private void typeFace() {
        gridLayoutManager = new GridLayoutManager(this,2);
        adapterMainLiveBroadcaster = new AdapterMainLiveBroadcaster(this,dataStreams);
        onLiveRecyclerView.setLayoutManager(gridLayoutManager);
        onLiveRecyclerView.setAdapter(adapterMainLiveBroadcaster);
        tvNoBroadCaster.setTypeface(appTypeface.getHind_semiBold());

    }
    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.get().setConnectivityListener(this);
    }

    @OnClick({R.id.tvVideoBroadCaster})
    public void onViewClicked(View v)
    {
        switch (v.getId())
        {
            case R.id.tvVideoBroadCaster:
                openVideoBroadcaster();
                break;

        }
    }
    public void openVideoBroadcaster() {
      //  Intent i = new Intent(this, LiveVideoBroadcasterActivity.class);
        Intent i = new Intent(this, PreviewActivity.class);
        startActivity(i);
    }

    public void openVideoPlayer() {
        Intent i = new Intent(this,LiveVideoPlayer.class);//LiveVideoPlayerActivity
        startActivity(i);
    }

    @Override
    public void liveBroadCasterData(ArrayList<DataStreams> streams)
    {

        dataStreams.clear();
        dataStreams.addAll(streams);

        if(streams.size()>0)
        {
            tvNoBroadCaster.setVisibility(View.GONE);
            onLiveRecyclerView.setVisibility(View.VISIBLE);
            adapterMainLiveBroadcaster.notifyDataSetChanged();
        }else
        {
            tvNoBroadCaster.setVisibility(View.VISIBLE);
            onLiveRecyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void showAlert(String message) {

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        presenterMain.checkForNetwork(isConnected);

    }

    @Override
    public void openNetworkScreen() {

    }
}
