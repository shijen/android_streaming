package com.livestreaming.newlivestream.ResponcePojo;

import java.io.Serializable;

/**
 * Created by Ali on 11/21/2018.
 */
public class SignUpLoginResponse implements Serializable
{
    private String message;
    private SignUpLoginData data;

    public String getMessage() {
        return message;
    }

    public SignUpLoginData getData() {
        return data;
    }
}
