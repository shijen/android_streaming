package com.livestreaming.newlivestream.signUpPage;

import com.livestreaming.newlivestream.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * Created by Ali on 11/21/2018.
 */
@Module
public interface SignUpActivityModule {

    @ActivityScoped
    @Binds
    SignUpActivityPresenterImpl.SignUpPresenter providePresenter(SignUpActivityPresenter signUpActivityPresenter);

    @ActivityScoped
    @Binds
    SignUpActivityPresenterImpl.SignUpView provideView(SignUpActivity signUpActivity);

}
