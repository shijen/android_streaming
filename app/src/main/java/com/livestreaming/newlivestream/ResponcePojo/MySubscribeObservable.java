package com.livestreaming.newlivestream.ResponcePojo;

import android.util.Log;

import io.reactivex.Observable;
import io.reactivex.Observer;

/**
 * Created by Ali on 12/19/2018.
 */
public class MySubscribeObservable extends Observable<SubscribedData> {

    private Observer<? super SubscribedData> observer;
    private static MySubscribeObservable observebleClass;

    public static MySubscribeObservable getInstance() {
        if (observebleClass == null) {
            observebleClass = new MySubscribeObservable();

            return observebleClass;
        } else {
            return observebleClass;
        }
    }

    @Override
    protected void subscribeActual(Observer<? super SubscribedData> observer) {
        this.observer = observer;
    }

    public void emitJobStatus(SubscribedData myBookingStatus)
    {
        Log.d("TAG", "emitJobStatus: "+myBookingStatus.getAction());
        observer.onNext(myBookingStatus);
        observer.onComplete();
    }
}
