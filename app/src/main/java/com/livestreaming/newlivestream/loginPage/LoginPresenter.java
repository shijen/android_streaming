package com.livestreaming.newlivestream.loginPage;

import com.livestreaming.newlivestream.utility.BasePresenter;
import com.livestreaming.newlivestream.utility.BaseViewImpl;

/**
 * Created by Ali on 11/21/2018.
 */
public interface LoginPresenter
{
    interface LogInPresent extends BasePresenter
    {

        void callSignInApi(String email);
    }
    interface LogInView extends BaseViewImpl
    {

        void moveToMainActivity();
    }
}
