package com.livestreaming.newlivestream.liveVideoPlayerStreamsLive.liveVideoBroadcaster;

import com.livestreaming.newlivestream.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * Created by Ali on 12/3/2018.
 */
@Module
public interface LiveBroadCastModule
{

    @ActivityScoped
    @Binds
    LiveBroadCastPresenterImpl.LiveBroadCastPresenter providePresenter(LiveBroadCastPresenter liveBroadCastPresenter);

    @ActivityScoped
    @Binds
    LiveBroadCastPresenterImpl.LiveBroadCastView provideLiveView(LiveVideoBroadcasterActivity liveVideoBroadcasterActivity);
}
