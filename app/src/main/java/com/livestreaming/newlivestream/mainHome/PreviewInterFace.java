package com.livestreaming.newlivestream.mainHome;

/**
 * Created by Ali on 12/17/2018.
 */
public interface PreviewInterFace
{
    interface PreViewPresenter
    {
        void onLiveBroadCastApi(String streamId, String streamType, String thumbnail, String streamName);
    }
    interface PreviewView
    {
        void onSuccess(String streamId);
        void onError(String error);
    }
}
