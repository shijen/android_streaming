package com.livestreaming.newlivestream.liveVideoPlayerStreamsLive.liveVideoBroadcaster;

import com.livestreaming.newlivestream.ResponcePojo.Messages;
import com.livestreaming.newlivestream.ResponcePojo.SubscribedData;
import com.livestreaming.newlivestream.utility.BasePresenter;
import com.livestreaming.newlivestream.utility.BaseViewImpl;

/**
 * Created by Ali on 12/3/2018.
 */
public interface LiveBroadCastPresenterImpl
{
    interface LiveBroadCastPresenter extends BasePresenter
    {
        void onLiveBroadCastApi(String streamId, String streamType, String thumbnail, String streamName);
        void onLiveChat(String streamId, String msg);

        void onChatRxJava();

        void initializeRxJAva();

    }
    interface LiveBroadCastView extends BaseViewImpl
    {

        void onSuccess(String streamId);

        void onMessageReceived(Messages messages);

        void onDataSubscribed(SubscribedData subscribedData);
    }
}
