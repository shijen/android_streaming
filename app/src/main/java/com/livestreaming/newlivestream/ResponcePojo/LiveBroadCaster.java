package com.livestreaming.newlivestream.ResponcePojo;

import java.io.Serializable;

/**
 * Created by Ali on 12/3/2018.
 */
public class LiveBroadCaster implements Serializable
{
    /*{"message":"Success.","data":{"streams":[]}}*/

    private String message;
    private LiveBroadCasterData data;

    public String getMessage() {
        return message;
    }

    public LiveBroadCasterData getData() {
        return data;
    }
}
