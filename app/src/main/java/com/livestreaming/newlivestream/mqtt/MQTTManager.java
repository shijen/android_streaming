package com.livestreaming.newlivestream.mqtt;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.livestreaming.newlivestream.R;
import com.livestreaming.newlivestream.ResponcePojo.DataStreams;
import com.livestreaming.newlivestream.ResponcePojo.Messages;
import com.livestreaming.newlivestream.ResponcePojo.MyChatObservable;
import com.livestreaming.newlivestream.ResponcePojo.MyStreamObservable;
import com.livestreaming.newlivestream.ResponcePojo.MySubscribeObservable;
import com.livestreaming.newlivestream.ResponcePojo.SubscribedData;
import com.livestreaming.newlivestream.utility.SessionManagerImpl;
import com.livestreaming.newlivestream.utility.VariableConstant;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

/**
 * <h1>MQTTManager</h1>
 * This class is used to handle the MQTT data
 * @author 3Embed
 * @since on 21-12-2017.
 */
public class MQTTManager
{
    private static final String TAG = "MQTTManager";
    private IMqttActionListener mMQTTListener;
    private MqttAndroidClient mqttAndroidClient;
    private MqttConnectOptions mqttConnectOptions;
    private Context mContext;
 //   private static volatile ArrayList<ProviderData> providerResponseData = new ArrayList<>();
    private String modelResponce = "";
    private Gson gson;
    private SessionManagerImpl sessionManagerImpl;
 //   private ProviderObservable rxProviderObserver;
//    private MyBookingObservable myObservable;

    @Inject
    public MQTTManager(Context context, SessionManagerImpl sessionManagerImpl, Gson gson
    )
   /* public MQTTManager(Context context, SessionManagerImpl sessionManagerImpl, Gson gson
    , MyBookingObservable myObservable, ProviderObservable rxProviderObserver)*/
            //,MyBookingObservable myObservable
    {
        mContext = context;
        this.gson = gson;
   //     this.myObservable = myObservable;
   //     this.rxProviderObserver=rxProviderObserver;
        this.sessionManagerImpl=sessionManagerImpl;
        mMQTTListener = new IMqttActionListener()
        {
            @Override
            public void onSuccess(IMqttToken asyncActionToken)
            {
                // subscribeToTopic(preferenceHelperDataSource.getMqttTopic());
                Log.w(TAG," onSuccessPhone: myqtt client "+asyncActionToken.isComplete());
            }
            @Override
            public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                exception.printStackTrace();
                Log.e(TAG, "onFailure: myqtt client "+asyncActionToken.isComplete()
                        +" "+exception.getMessage());
            }
        };
    }

    /**
     * <h2>subscribeToTopic</h2>
     * This method is used to subscribe to the mqtt topic
     */
    public void subscribeToTopic(String mqttTopic, int qos)
    {
        try
        {
            if (mqttAndroidClient != null)
                mqttAndroidClient.subscribe(mqttTopic, qos);
        }
        catch (MqttException e)
        {
            Log.e(TAG," MqttException "+e);
            e.printStackTrace();
        }
    }

    /**
     * <h2>unSubscribeToTopic</h2>
     * This method is used to unSubscribe to topic already subscribed
     * @param topic Topic name from which to  unSubscribe
     */
    @SuppressWarnings("TryWithIdenticalCatches")
    public void unSubscribeToTopic(String topic)
    {
        try
        {
            if (mqttAndroidClient != null)
                mqttAndroidClient.unsubscribe(topic);
        }
        catch (MqttException e)
        {
            e.printStackTrace();
        }
        catch (NullPointerException e)
        {
            e.printStackTrace();
        }
    }

    public void onModelResponseSet()
    {
        modelResponce = "";
    }

    /**
     * <h2>isMQTTConnected</h2>
     * This method is used to check whether MQTT is connected
     * @return boolean value whether MQTT is connected
     */
    public boolean isMQTTConnected()
    {
            return mqttAndroidClient != null && mqttAndroidClient.isConnected();
    }

    /**
     * <h2>createMQttConnection</h2>
     * This method is used to create the connection with MQTT
     * @param clientId customer ID to connect MQTT
     */
    @SuppressWarnings("unchecked")
    public void createMQttConnection(String clientId)
    {

        Log.w(TAG," createMQttConnection: "+clientId+(System.currentTimeMillis()/1000));
        String serverUri = VariableConstant.MQTT_URL_HOST + ":" + VariableConstant.MQTT_PORT;
     //   MqttClient.generateClientId();
        mqttAndroidClient = new MqttAndroidClient(mContext, serverUri, clientId+(System.currentTimeMillis()/1000));
        mqttAndroidClient.setCallback(new MqttCallback() {
            @Override
            public void connectionLost(Throwable cause) {
              //  Log.w(TAG," connectionLost: "+cause.getMessage());
            }
            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception
            {
                String topicSplit[] = topic.split("/");
                if(topicSplit[0].equals(MqttEvents.StreamNow.value))
                {
                    Log.w(TAG," handleNewVehicleTypesData Provider: "+new String(message.getPayload()));
                    handleNewVehicleTypesData(new String(message.getPayload()));
                }
                else if(topicSplit[0].equals(MqttEvents.StreamSubscrib.value))
                {
                    Log.w(TAG," handleNewVehicleTypesData StreamSubscrib: "+new String(message.getPayload()));
                    handleLiveBookingStatus(new String(message.getPayload()));

                }else if(topicSplit[0].equals(MqttEvents.StreamChat.value))
                {
                    Log.w(TAG," handleNewVehicleTypesData StreamChat: "+new String(message.getPayload()));
                    handleNewMsg(new String(message.getPayload()));
                }
                Log.w(TAG," handleNewVehicleTypesData Topic: "+topic);

            }
            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {
                Log.w(TAG," deliveryComplete: "+token);
            }
        });

       /* mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setCleanSession(false);
        mqttConnectOptions.setAutomaticReconnect(true);*/
        byte[] payload = sessionManagerImpl.getSID().getBytes();
        mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setCleanSession(false);
        mqttConnectOptions.setAutomaticReconnect(true);
        mqttConnectOptions.setKeepAliveInterval(60);
        mqttConnectOptions.setUserName(mContext.getString(R.string.MQTT_USERNAME));
        mqttConnectOptions.setPassword(mContext.getString(R.string.MQTT_PASSWORD).toCharArray());
        mqttConnectOptions.setWill(MqttEvents.WillTopic.value,payload,1,false);
        connectMQTTClient(mContext);
    }

    private void handleNewMsg(String jsonObject) {


        Log.d("MESSAGE", "managerMqttMESSGE: "+jsonObject);

        try
        {
            String jsonMsg  = new JSONObject(jsonObject).getJSONObject("data").toString();

            Messages messages = gson.fromJson(jsonMsg,Messages.class);
            MyChatObservable.getInstance().emitJobStatus(messages);

        }catch (Exception e)
        {
            e.printStackTrace();
        }

       // ChatReponce chatReponce = gson.fromJson(jsonObject,ChatReponce.class);


    }

    private void handleLiveBookingStatus(String jsonObject)
    {

        Log.d("LIVETRACK", "managerMqttLIVE: " + jsonObject);
        try {
            String jsonData = new JSONObject(jsonObject).getJSONObject("data").toString();
            SubscribedData subscribedData = gson.fromJson(jsonData,SubscribedData.class);
            MySubscribeObservable.getInstance().emitJobStatus(subscribedData);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //    LiveTackPojo liveTrackPojo = new Gson().fromJson(jsonObject, LiveTackPojo.class);

    }



    /**
     * <h1>handleNewVehicleTypesData</h1>
     * <p>
     *     method to handle & parse config data received from pubnub
     *     and also update the views if it has been changed from
     * </p>
     */
    public void handleNewVehicleTypesData(String configDataNew)
    {

        try
        {
           // ProviderResponse mqttResponseData = gson.fromJson(configDataNew, ProviderResponse.class);

            if(configDataNew!=null)
            {
                String dataRes = new JSONObject(configDataNew).getJSONObject("data").toString();
                Log.d(TAG, "handleNewVehicleTypesDataIs: "+dataRes);
                DataStreams dataStreams = gson.fromJson(dataRes,DataStreams.class);
                MyStreamObservable.getInstance().emitJobStatus(dataStreams);

            }


        }
        catch (Exception exc)
        {
            exc.printStackTrace();
            Log.d(TAG, "handleNewVehicleTypesData() exc: "+exc);
        }
    }
    /**
     * <h2>connectMQTTClient</h2>
     * This method is used to connect to MQTT client
     */
    private void connectMQTTClient(Context mContext)
    {
        try
        {
            Log.w(TAG," connectMQTTClient: ");
            mqttAndroidClient.connect(mqttConnectOptions, mContext, mMQTTListener);
        }
        catch (MqttException e)
        {
            Log.e(TAG," MqttException: "+e);
            e.printStackTrace();
        }
    }

    /**
     * <h2>disconnect</h2>
     * This method is used To disconnect the MQtt client
     */
    public void disconnect(String mqttTopic)
    {
        try
        {
            if (mqttAndroidClient != null)
            {
              //  unSubscribeToTopic(mqttTopic);
                mqttAndroidClient.disconnect();
            }
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("TryWithIdenticalCatches")
    public void publish(String topicName, JSONObject obj, int qos, boolean retained) {

        try {
            mqttAndroidClient.publish(topicName, obj.toString().getBytes(), qos, retained);

        } catch (MqttException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

}
