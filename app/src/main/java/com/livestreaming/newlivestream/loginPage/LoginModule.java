package com.livestreaming.newlivestream.loginPage;

import com.livestreaming.newlivestream.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * Created by Ali on 11/20/2018.
 */
@Module
public interface LoginModule
{
    @ActivityScoped
    @Binds
    LoginPresenter.LogInPresent providePresenter(LoginPresenterImpl loginPresenter);

    @ActivityScoped
    @Binds
    LoginPresenter.LogInView provideView(LonginActivity longinActivity);
}
