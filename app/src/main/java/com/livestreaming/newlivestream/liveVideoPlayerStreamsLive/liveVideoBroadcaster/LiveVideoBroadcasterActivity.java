package com.livestreaming.newlivestream.liveVideoPlayerStreamsLive.liveVideoBroadcaster;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.net.Uri;
import android.opengl.GLSurfaceView;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.livestreaming.newlivestream.R;
import com.livestreaming.newlivestream.ResponcePojo.Messages;
import com.livestreaming.newlivestream.ResponcePojo.SubscribedData;
import com.livestreaming.newlivestream.mainHome.MessageAdapter;
import com.livestreaming.newlivestream.mqtt.MQTTManager;
import com.livestreaming.newlivestream.mqtt.MqttEvents;
import com.livestreaming.newlivestream.utility.SessionManagerImpl;
import com.livestreaming.newlivestream.utility.Utility;

import java.io.File;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;
import io.antmedia.android.broadcaster.ILiveVideoBroadcaster;
import io.antmedia.android.broadcaster.LiveVideoBroadcaster;
import io.antmedia.android.broadcaster.utils.Resolution;

import static com.livestreaming.newlivestream.mainHome.MainActivity.RTMP_BASE_URL;

public class LiveVideoBroadcasterActivity extends DaggerAppCompatActivity implements LiveBroadCastPresenterImpl.LiveBroadCastView {


    private static final String TAG = LiveVideoBroadcasterActivity.class.getSimpleName();
    private ViewGroup mRootView;
    boolean mIsRecording = false;
    private EditText etSendYourText;
    private Timer mTimer,mTimerBrodCast;
    private long mElapsedTime;
    public TimerHandler mTimerHandler;
    private ImageButton mSettingsButton;
    private ImageView sendMessage;
    private CameraResolutionsFragment mCameraResolutionsDialog;
    private Intent mLiveVideoBroadcasterServiceIntent;
    private TextView mStreamLiveStatus,stream_live_close,tvNoOfView;
    private GLSurfaceView mGLView;
    private ILiveVideoBroadcaster mLiveVideoBroadcaster;
   // private Button mBroadcastControlButton;
    //  private ImageView takePicTure;
    private LinearLayout llStartStreaming;//llCameraTakePic
    @Inject LiveBroadCastPresenterImpl.LiveBroadCastPresenter presenter;
    private String streamName = "rtmpstream-";
    private static final String IMAGE_DIRECTORY = "/YourDirectName";
    File wallpaperDirectory;
    File fImage;
    private String streamId = "",streamType,thumbnail  = "";

    @Inject
    MQTTManager mqttManager;
    @Inject
    SessionManagerImpl manager;

    private RecyclerView recyclerViewMesg;

    private MessageAdapter messageAdapter;
    private ArrayList<Messages> messages = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;


    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            LiveVideoBroadcaster.LocalBinder binder = (LiveVideoBroadcaster.LocalBinder) service;
            if (mLiveVideoBroadcaster == null) {
                mLiveVideoBroadcaster = binder.getService();
                mLiveVideoBroadcaster.init(LiveVideoBroadcasterActivity.this, mGLView);
                mLiveVideoBroadcaster.setAdaptiveStreaming(true);
            }
            mLiveVideoBroadcaster.openCamera(Camera.CameraInfo.CAMERA_FACING_FRONT);
        }
        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mLiveVideoBroadcaster = null;
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Hide title
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //binding on resume not to having leaked service connection
        mLiveVideoBroadcasterServiceIntent = new Intent(this, LiveVideoBroadcaster.class);
        //this makes service do its job until done
        startService(mLiveVideoBroadcasterServiceIntent);

        setContentView(R.layout.activity_live_video_broadcaster);


        mTimerHandler = new TimerHandler();
        etSendYourText = findViewById(R.id.etSendYourText);
        sendMessage = findViewById(R.id.sendMessage);
        //  takePicTure =  findViewById(R.id.takePicTure);
        //  llCameraTakePic =  findViewById(R.id.llCameraTakePic);
        llStartStreaming =  findViewById(R.id.llStartStreaming);
        llStartStreaming.setVisibility(View.GONE);
        //   llCameraTakePic.setVisibility(View.VISIBLE);

        recyclerViewMesg = findViewById(R.id.recyclerViewMesg);
        mRootView = (ViewGroup)findViewById(R.id.root_layout);
        mSettingsButton = (ImageButton)findViewById(R.id.settings_button);
        mStreamLiveStatus = (TextView) findViewById(R.id.stream_live_status);
        stream_live_close = (TextView) findViewById(R.id.stream_live_close);
        tvNoOfView = (TextView) findViewById(R.id.tvNoOfView);


     //   mBroadcastControlButton = (Button) findViewById(R.id.toggle_broadcasting);

        // Configure the GLSurfaceView.  This will start the Renderer thread, with an
        // appropriate EGL activity.
        mGLView = (GLSurfaceView) findViewById(R.id.cameraPreview_surfaceView);
        if (mGLView != null) {
            mGLView.setEGLContextClientVersion(2);     // select GLES 2.0
        }
        stream_live_close.setOnClickListener(onCliseClick());
        getIntentValue();

//        takePicTure.setOnClickListener(onClickTakePic());

        sendMessage.setOnClickListener(onClickSend());

        InitializeAdapter();
        presenter.onChatRxJava();
    }

    private void InitializeAdapter() {
        linearLayoutManager = new LinearLayoutManager(this);
        messageAdapter = new MessageAdapter(messages,this);
        recyclerViewMesg.setLayoutManager(linearLayoutManager);
        recyclerViewMesg.setAdapter(messageAdapter);
    }

    private View.OnClickListener onClickSend() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!etSendYourText.getText().toString().trim().equals(""))
                {
                    String msg = etSendYourText.getText().toString().trim();
                    presenter.onLiveChat(streamId,msg);
                    etSendYourText.setText("");
                    Utility.hideKeyboard(LiveVideoBroadcasterActivity.this);
                   /* String name = manager.getFirstName() +" "+manager.getLastName();
                    Messages message = new Messages(msg,manager.getSID(),name,manager.getProfilePicUrl());
                    messages.add(message);
                    messageAdapter.notifyDataSetChanged();*/
                }
            }
        };
    }

    private View.OnClickListener onCliseClick() {
        Log.d(TAG, "onCliseClick: "+stream_live_close);
        return view -> onBackPressed();
    }

    private void getIntentValue() {
        if(getIntent().getExtras()!=null)
        {
            streamName = getIntent().getStringExtra("StreamName");
            streamType = getIntent().getStringExtra("StreamType");
            streamId = getIntent().getStringExtra("StreamId");
            thumbnail = getIntent().getStringExtra("ThumbNail");
            Log.d(TAG, "getIntentValueStreamId: "+streamId);
        }
        mTimerBrodCast = new Timer();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startPublishing();
            }
        }, 2000);

        createMqTTManager();

    }

    private void createMqTTManager() {

        if(!mqttManager.isMQTTConnected())
        {
            mqttManager.createMQttConnection(manager.getSID());
        }
        presenter.initializeRxJAva();
    }

    private void startPublishing()
    {

        if(mTimerBrodCast == null) {
            return;
        }

        mTimerBrodCast.scheduleAtFixedRate(new TimerTask() {

            public void run() {
               /* mElapsedTime += 1; //increase every sec
                mTimerHandler.obtainMessage(TimerHandler.INCREASE_TIMER).sendToTarget();

                if (mLiveVideoBroadcaster == null || !mLiveVideoBroadcaster.isConnected()) {
                    mTimerHandler.obtainMessage(TimerHandler.CONNECTION_LOST).sendToTarget();
                }*/
                startBroadCasting();
            }
        }, 0, 1000);



    }

    public void changeCamera(View v) {
        if (mLiveVideoBroadcaster != null) {
            mLiveVideoBroadcaster.changeCamera();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        //this lets activity bind
        bindService(mLiveVideoBroadcasterServiceIntent, mConnection, 0);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LiveVideoBroadcaster.PERMISSIONS_REQUEST: {
                if (mLiveVideoBroadcaster.isPermissionGranted()) {
                    mLiveVideoBroadcaster.openCamera(Camera.CameraInfo.CAMERA_FACING_BACK);
                }
                else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.CAMERA) ||
                            ActivityCompat.shouldShowRequestPermissionRationale(this,
                                    Manifest.permission.RECORD_AUDIO) || ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE) ) {
                        mLiveVideoBroadcaster.requestPermission();
                    }
                    else {
                        new AlertDialog.Builder(LiveVideoBroadcasterActivity.this)
                                .setTitle(R.string.permission)
                                .setMessage(getString(R.string.app_doesnot_work_without_permissions))
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                        try {
                                            //Open the specific App Info page:
                                            Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                            intent.setData(Uri.parse("package:" + getApplicationContext().getPackageName()));
                                            startActivity(intent);

                                        } catch ( ActivityNotFoundException e ) {
                                            //e.printStackTrace();

                                            //Open the generic Apps page:
                                            Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS);
                                            startActivity(intent);

                                        }
                                    }
                                }).show();
                    }
                }
                return;
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "onPause");

        //hide dialog if visible not to create leaked window exception
        if (mCameraResolutionsDialog != null && mCameraResolutionsDialog.isVisible()) {
            mCameraResolutionsDialog.dismiss();
        }
        mLiveVideoBroadcaster.pause();
    }


    @Override
    protected void onStop() {
        super.onStop();
        unbindService(mConnection);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE || newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            mLiveVideoBroadcaster.setDisplayOrientation();
        }

    }

    public void showSetResolutionDialog(View v) {

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragmentDialog = getSupportFragmentManager().findFragmentByTag("dialog");
        if (fragmentDialog != null) {

            ft.remove(fragmentDialog);
        }

        ArrayList<Resolution> sizeList = mLiveVideoBroadcaster.getPreviewSizeList();


        if (sizeList != null && sizeList.size() > 0) {
            mCameraResolutionsDialog = new CameraResolutionsFragment();

            mCameraResolutionsDialog.setCameraResolutions(sizeList, mLiveVideoBroadcaster.getPreviewSize());
            mCameraResolutionsDialog.show(ft, "resolutiton_dialog");
        }
        else {
            Snackbar.make(mRootView, "No resolution available",Snackbar.LENGTH_LONG).show();
        }

    }

    public void toggleBroadcasting(View v) {
        startBroadCasting();
    }

    private void startBroadCasting()
    {
        if (!mIsRecording)
        {
            if (mLiveVideoBroadcaster != null) {
                if (!mLiveVideoBroadcaster.isConnected()) {
                    mTimerBrodCast.purge();
                    mTimerBrodCast.cancel();
                    Log.d(TAG, "startBroadCasting: "+streamName);
                    mTimerBrodCast = null;

                    mqttManager.subscribeToTopic(MqttEvents.StreamSubscrib.value+"/"+streamId,1);
                    mqttManager.subscribeToTopic(MqttEvents.StreamChat.value+"/"+streamId,1);
                    //       String streamName = mStreamNameEditText.getText().toString();

                    new AsyncTask<String, String, Boolean>() {
                        ContentLoadingProgressBar
                                progressBar;
                        @Override
                        protected void onPreExecute() {
                            progressBar = new ContentLoadingProgressBar(LiveVideoBroadcasterActivity.this);
                            progressBar.show();
                        }

                        @Override
                        protected Boolean doInBackground(String... url) {
                            return mLiveVideoBroadcaster.startBroadcasting(url[0]);

                        }

                        @Override
                        protected void onPostExecute(Boolean result) {
                            progressBar.hide();
                            mIsRecording = result;
                            if (result) {
                                mStreamLiveStatus.setVisibility(View.VISIBLE);
                                llStartStreaming.setVisibility(View.VISIBLE);
                           //     mBroadcastControlButton.setText(R.string.stop_broadcasting);
                                mSettingsButton.setVisibility(View.GONE);
                                startTimer();//start the recording duration
                            }
                            else {
                                Snackbar.make(mRootView, R.string.stream_not_started, Snackbar.LENGTH_LONG).show();
                                triggerStopRecording();
                            }
                        }
                    }.execute(RTMP_BASE_URL + streamName);
                }
                else {
                    Snackbar.make(mRootView, R.string.streaming_not_finished, Snackbar.LENGTH_LONG).show();
                }
            }
            else {
                Snackbar.make(mRootView, R.string.oopps_shouldnt_happen, Snackbar.LENGTH_LONG).show();
            }
        }
        else
        {
            triggerStopRecording();
        }
    }


    public void triggerStopRecording() {
        if (mIsRecording) {
           // mBroadcastControlButton.setText(R.string.start_broadcasting);

            mStreamLiveStatus.setVisibility(View.GONE);
            mStreamLiveStatus.setText(R.string.live_indicator);
            mSettingsButton.setVisibility(View.VISIBLE);

            stopTimer();
            mLiveVideoBroadcaster.stopBroadcasting();
        }

        mIsRecording = false;
    }

    //This method starts a mTimer and updates the textview to show elapsed time for recording
    public void startTimer() {

        if(mTimer == null) {
            mTimer = new Timer();
        }

        mElapsedTime = 0;
        mTimer.scheduleAtFixedRate(new TimerTask() {

            public void run() {
                mElapsedTime += 1; //increase every sec
                mTimerHandler.obtainMessage(TimerHandler.INCREASE_TIMER).sendToTarget();

                if (mLiveVideoBroadcaster == null || !mLiveVideoBroadcaster.isConnected()) {
                    mTimerHandler.obtainMessage(TimerHandler.CONNECTION_LOST).sendToTarget();
                }
            }
        }, 0, 1000);
    }


    public void stopTimer()
    {
        if (mTimer != null) {
            this.mTimer.cancel();
        }
        this.mTimer = null;
        this.mElapsedTime = 0;
    }

    public void setResolution(Resolution size) {
        mLiveVideoBroadcaster.setResolution(size);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void apiError(String errorMsg) {

    }

    @Override
    public void onNetworkRetry(boolean isNetwork, String thisApi) {

    }



    private class TimerHandler extends Handler {
        static final int CONNECTION_LOST = 2;
        static final int INCREASE_TIMER = 1;

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case INCREASE_TIMER:
                    mStreamLiveStatus.setText(getString(R.string.live_indicator) + " - " + getDurationString((int) mElapsedTime));
                    break;
                case CONNECTION_LOST:
                    triggerStopRecording();
                    new AlertDialog.Builder(LiveVideoBroadcasterActivity.this)
                            .setMessage(R.string.broadcast_connection_lost)
                            .setPositiveButton(android.R.string.yes, null)
                            .show();

                    break;
            }
        }
    }

    public static String getDurationString(int seconds) {

        if(seconds < 0 || seconds > 2000000)//there is an codec problem and duration is not set correctly,so display meaningfull string
            seconds = 0;
        int hours = seconds / 3600;
        int minutes = (seconds % 3600) / 60;
        seconds = seconds % 60;

        if(hours == 0)
            return twoDigitString(minutes) + " : " + twoDigitString(seconds);
        else
            return twoDigitString(hours) + " : " + twoDigitString(minutes) + " : " + twoDigitString(seconds);
    }

    public static String twoDigitString(int number) {

        if (number == 0) {
            return "00";
        }

        if (number / 10 == 0) {
            return "0" + number;
        }

        return String.valueOf(number);
    }

    @Override
    public void onSuccess(String streamId) {
        //  llStartStreaming.setVisibility(View.VISIBLE);
        //   llCameraTakePic.setVisibility(View.VISIBLE);
        finish();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if(!mIsRecording)
        {
            finish();
        }else
        {
            triggerStopRecording();
            streamType = "stop";
            presenter.onLiveBroadCastApi(streamId,streamType,thumbnail,streamName);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mTimerBrodCast!=null)
        {
            mTimerBrodCast.purge();
            mTimerBrodCast.cancel();
        }
        if(mqttManager.isMQTTConnected())
        {
            mqttManager.unSubscribeToTopic(MqttEvents.StreamSubscrib.value+"/"+streamId);
            mqttManager.unSubscribeToTopic(MqttEvents.StreamChat.value+"/"+streamId);
        }

        Log.d(TAG, "startBroadCasting: "+streamName);
    }

    @Override
    public void onMessageReceived(Messages message) {

        messages.add(message);
        messageAdapter.notifyDataSetChanged();
        scrollToBottom();

    }
    private void scrollToBottom() {
        recyclerViewMesg.scrollToPosition(messageAdapter.getItemCount() - 1);
    }

    @Override
    public void onDataSubscribed(SubscribedData subscribedData) {

        String noOfView = subscribedData.getActiveViewwers()+"";
        tvNoOfView.setText(noOfView);
    }
}
