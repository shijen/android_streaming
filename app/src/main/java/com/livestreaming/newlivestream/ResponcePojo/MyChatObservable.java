package com.livestreaming.newlivestream.ResponcePojo;

import android.util.Log;

import io.reactivex.Observable;
import io.reactivex.Observer;

/**
 * Created by Ali on 12/19/2018.
 */
public class MyChatObservable extends Observable<Messages> {

    private Observer<? super Messages> observer;
    private static MyChatObservable observebleClass;

    public static MyChatObservable getInstance() {
        if (observebleClass == null) {
            observebleClass = new MyChatObservable();

            return observebleClass;
        } else {
            return observebleClass;
        }
    }

    @Override
    protected void subscribeActual(Observer<? super Messages> observer) {
        this.observer = observer;
    }

    public void emitJobStatus(Messages myBookingStatus)
    {
        Log.d("TAG", "emitJobStatus: "+myBookingStatus.getUserName());
        observer.onNext(myBookingStatus);
        observer.onComplete();
    }
}
