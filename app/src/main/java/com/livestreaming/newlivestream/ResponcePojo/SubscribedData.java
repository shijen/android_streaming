package com.livestreaming.newlivestream.ResponcePojo;

import java.io.Serializable;

/**
 * Created by Ali on 12/24/2018.
 */
public class SubscribedData implements Serializable
{
    /*{"data":{"id":"5c1a5cd7b995d95f2f494ac2","name":"akbar","action":"subscribe","subscribersCount":1,
"activeViewwers":1}}
*/

    private String id,name,action;
    private int subscribersCount,activeViewwers;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAction() {
        return action;
    }

    public int getSubscribersCount() {
        return subscribersCount;
    }

    public int getActiveViewwers() {
        return activeViewwers;
    }
}
