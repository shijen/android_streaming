package com.livestreaming.newlivestream.mainHome;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.livestreaming.newlivestream.R;
import com.livestreaming.newlivestream.ResponcePojo.DataStreams;
import com.livestreaming.newlivestream.liveVideoPlayerStreamsLive.liveVideoPlayer.LiveVideoPlayer;

import java.util.ArrayList;

/**
 * Created by Ali on 12/18/2018.
 */
public class AdapterMainLiveBroadcaster extends RecyclerView.Adapter {

    private Context mContext;
    private ArrayList<DataStreams>dataStreams;


    public AdapterMainLiveBroadcaster(Context mContext, ArrayList<DataStreams>dataStreams) {
        this.mContext = mContext;
        this.dataStreams = dataStreams;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.main_list_adapter,viewGroup,false);
        return new MainLiveHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        MainLiveHolder holder = (MainLiveHolder) viewHolder;

        holder.tvBroadCasterName.setText(dataStreams.get(i).getUserName());
        //http://s3-ap-southeast-1.amazonaws.com/appscrip/LiveStream/thumbNail/1545401039953.jpg

        String thumbNail;
        if(!dataStreams.get(i).getThumbnail().contains("http"))
             thumbNail = "http:"+dataStreams.get(i).getThumbnail();
        else
            thumbNail = dataStreams.get(i).getThumbnail();
        Log.d("TAG", "onBindViewHolder: "+dataStreams.get(i).getThumbnail()
        +" thumb "+thumbNail);
        Glide.with(mContext)
                .load(thumbNail)
                .into(holder.ivBroadcasterThumbNail);

    }

    @Override
    public int getItemCount() {
        return dataStreams.size();
    }

    class MainLiveHolder extends RecyclerView.ViewHolder {
        TextView tvBroadCasterName;
        ImageView ivBroadcasterThumbNail;
        public MainLiveHolder(@NonNull View itemView) {
            super(itemView);
            tvBroadCasterName = itemView.findViewById(R.id.tvBroadCasterName);
            ivBroadcasterThumbNail = itemView.findViewById(R.id.ivBroadcasterThumbNail);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mContext, LiveVideoPlayer.class);
                    intent.putExtra("streamName",dataStreams.get(getAdapterPosition()).getName());
                    intent.putExtra("streamId",dataStreams.get(getAdapterPosition()).getId());
                    mContext.startActivity(intent);
                }
            });
        }
    }
}
