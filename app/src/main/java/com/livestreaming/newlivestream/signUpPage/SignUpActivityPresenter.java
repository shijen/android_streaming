package com.livestreaming.newlivestream.signUpPage;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.livestreaming.newlivestream.ResponcePojo.CheckFlagForValidity;
import com.livestreaming.newlivestream.ResponcePojo.SignUpLoginResponse;
import com.livestreaming.newlivestream.dagger.ApiServices;
import com.livestreaming.newlivestream.utility.SessionManagerImpl;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.livestreaming.newlivestream.utility.VariableConstant.SEL_LANG;
import static com.livestreaming.newlivestream.utility.VariableConstant.SUCCESS;

/**
 * Created by Ali on 11/21/2018.
 */
public class SignUpActivityPresenter implements SignUpActivityPresenterImpl.SignUpPresenter {


    private String TAG = SignUpActivityPresenter.class.getSimpleName();
    private CheckFlagForValidity checkFlag;
    @Inject SignUpActivityPresenterImpl.SignUpView signUpView;

    @Inject
    ApiServices apiServices;

    @Inject
    Gson gson;

    @Inject
    SessionManagerImpl manager;


    @Inject
    public SignUpActivityPresenter()
    {
        checkFlag = new CheckFlagForValidity();
    }

    @Override
    public void validateFname(String firstName) {


        if (checkFlag.validateFname(firstName)) {
            signUpView.clearError(1);
            checkFlag.setnFlg(true);
        }else
        {
            signUpView.setFirstNameError();
            checkFlag.setnFlg(false);
        }
    }

    @Override
    public boolean validatePhone(String isDialog, String countryCode, String phone) {

        if (TextUtils.isEmpty(phone)) {
            signUpView.setMobileError(true);
            checkFlag.setmFlg(false);
            return false;
        } else
        {
            if (validatePhoneNumber(countryCode,phone)) {
                signUpView.setMobileErrorMsg("");
                checkFlag.setmFlg(true);
                return true;
                // phoneAlreadyExists(flag_show_dialog, countryCode, phone);
            } else
            {
                checkFlag.setmFlg(false);
                signUpView.setMobileError(false);
                return false;
                // signUpView.setMobileInvalid();
            }
        }

    }

    @Override
    public boolean validateEmail(String isDialog, String email) {

        if (TextUtils.isEmpty(email)) {
            signUpView.setEmailError(true);
            checkFlag.seteFlg(false);
            return false;
        } else
        {
            if (!checkFlag.validEmail(email)) {
                //  emailAlreadyExists(flag_to_dialog, email);
                signUpView.setEmailValid("");
                checkFlag.seteFlg(true);
                return true;
            } else {
                signUpView.setEmailError(false);
                checkFlag.seteFlg(false);
                return false;
            }
        }

    }

    String countryCode,name,email,phone;

    @Override
    public void signUpApi(String countryCode, String name, String email, String phone) {
        this.phone = phone;
        this.email = email;
        this.name = name;
        this.countryCode = countryCode;

        if(!checkFlag.isnFlg())
        {
            if (checkFlag.validateFname(name))
            {
                checkFlag.setnFlg(false);
                signUpView.setFirstNameError();
                signUpView.hideProgress();

            } else
            {
                checkFlag.setnFlg(true);
                signUpView.clearError(1);
                checkEmailFlag(email);
            }
        }
        else if(!checkFlag.iseFlg())
        {
            checkEmailFlag(email);

        } else if(!checkFlag.ismFlg())
        {

            checkMobileFlag();
        }else
        {
            callApi();
        }
    }



    private void checkEmailFlag(String email) {

        if (!checkFlag.iseFlg()) {
            if (validateEmail("n",email))
                checkMobileFlag();
            else
                signUpView.hideProgress();
        } else
            checkMobileFlag();
        //validatePhone("n",countryCode,phone);

    }
    private void checkMobileFlag()
    {
        if(!checkFlag.ismFlg())
        {
            if(validatePhone("n",countryCode,phone))
                callApi();
            else
                signUpView.hideProgress();
        }else
            callApi();

    }

    private void callApi()
    {
        Log.d(TAG, "callApi: "+SEL_LANG+" F "+name+" A  Em "+email+" countryCode "+countryCode+" phone "+phone);
        Observable<Response<ResponseBody>> observable = apiServices.signUpApi(SEL_LANG
                ,name,"A",email,countryCode,phone,1);

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {

                        int code = responseBodyResponse.code();
                        Log.d(TAG, "onNext: "+code);
                        String response;
                        signUpView.hideProgress();
                        SignUpLoginResponse signUpLoginResponse;
                        try{
                            switch (code)

                            {
                                case SUCCESS:
                                    response = responseBodyResponse.body().string();
                                     signUpLoginResponse = gson.fromJson(response,SignUpLoginResponse.class);
                                    manager.setFcmTopic(signUpLoginResponse.getData().getFcmTopic());
                                    manager.setAUTH(signUpLoginResponse.getData().getAuthToken());
                                    manager.setEmail(signUpLoginResponse.getData().getEmail());
                                    manager.setSID(signUpLoginResponse.getData().getId());
                                    manager.setCountryCode(signUpLoginResponse.getData().getPhone().getCountryCode());
                                    manager.setMobileNo(signUpLoginResponse.getData().getPhone().getPhone());
                                    manager.setFirstName(signUpLoginResponse.getData().getFirstName());
                                    signUpView.moveToMainActivity();
                                    break;
                                    default:
                                        response = responseBodyResponse.errorBody().string();
                                        signUpLoginResponse = gson.fromJson(response,SignUpLoginResponse.class);
                                        signUpView.apiError(signUpLoginResponse.getMessage());
                            }

                        }catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e)
                    {
                        e.printStackTrace();
                        signUpView.hideProgress();
                        signUpView.onNetworkRetry(true,"signUp");

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public boolean validatePhoneNumber(String countryCode, String mobileNumber)
    {
        String phoneNumberE164Format = countryCode.concat(mobileNumber);
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        try {
            Phonenumber.PhoneNumber phoneNumberProto = phoneUtil.parse(phoneNumberE164Format, null);
            boolean isValid = phoneUtil.isValidNumber(phoneNumberProto); // returns true if valid

            if (isValid  &&  ( phoneUtil.getNumberType(phoneNumberProto)== PhoneNumberUtil.PhoneNumberType.MOBILE||phoneUtil.getNumberType(phoneNumberProto)== PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE)  )
            {
                return true;
            }else
                return false;

        }catch (NumberParseException e){
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void connectView() {

    }

    @Override
    public void detachView() {

    }
}
