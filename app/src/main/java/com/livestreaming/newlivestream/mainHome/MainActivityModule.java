package com.livestreaming.newlivestream.mainHome;

import com.livestreaming.newlivestream.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * Created by Ali on 11/20/2018.
 */
@Module
public interface MainActivityModule
{
    @Binds
    @ActivityScoped
    MainActivityPresenterImpl.presenterMain provideMainPresenter(MainActivityPresenter mainPresenter);

    @Binds
    @ActivityScoped
    MainActivityPresenterImpl.ViewMain provideMainView(MainActivity mainPresenter);
}
