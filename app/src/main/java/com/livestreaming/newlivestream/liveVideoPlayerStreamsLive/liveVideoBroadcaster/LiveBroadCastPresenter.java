package com.livestreaming.newlivestream.liveVideoPlayerStreamsLive.liveVideoBroadcaster;

import android.util.Log;

import com.livestreaming.newlivestream.ResponcePojo.Messages;
import com.livestreaming.newlivestream.ResponcePojo.MyChatObservable;
import com.livestreaming.newlivestream.ResponcePojo.MySubscribeObservable;
import com.livestreaming.newlivestream.ResponcePojo.SubscribedData;
import com.livestreaming.newlivestream.dagger.ApiServices;
import com.livestreaming.newlivestream.utility.SessionManagerImpl;
import com.livestreaming.newlivestream.utility.VariableConstant;

import org.json.JSONObject;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by Ali on 12/3/2018.
 */
public class LiveBroadCastPresenter implements LiveBroadCastPresenterImpl.LiveBroadCastPresenter
{


    private static final String TAG = LiveBroadCastPresenter.class.getSimpleName();
    @Inject
    ApiServices apiServices;
    @Inject
    SessionManagerImpl manager;

    @Inject LiveBroadCastPresenterImpl.LiveBroadCastView view;


    @Inject
    public LiveBroadCastPresenter() {
    }

    @Override
    public void connectView() {

    }

    @Override
    public void detachView() {

    }

    @Override
    public void onLiveChat(String streamId, String msg)
    {
        Observable<Response<ResponseBody>> observer = apiServices.postApiChatStream(manager.getAUTH(), VariableConstant.SEL_LANG
                ,streamId,msg);

        observer.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {

                        int code = responseBodyResponse.code();
                        Log.d("onLiveChat", "onNext: "+code);
                        try
                        {
                            String response;
                            if(code == 200)
                            {
                                response = responseBodyResponse.body().string();
                                Log.d("onLiveChat", "onNextSUCC: "+response);
                            }else
                            {
                                response = responseBodyResponse.errorBody().string();
                                Log.d("onLiveChat", "onNextERR: "+response);
                            }


                        }catch (Exception e)

                        {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void onLiveBroadCastApi(String streamId, String streamType, String thumbnail, String streamName)
    {
       Observable<Response<ResponseBody>> observable = apiServices.postApiCallStream(manager.getAUTH(), VariableConstant.SEL_LANG
       ,streamId,streamType,streamName,thumbnail,false,false);

       observable.subscribeOn(Schedulers.io())
               .observeOn(AndroidSchedulers.mainThread())
               .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                   @Override
                   public void onNext(Response<ResponseBody> responseBodyResponse) {


                       int code = responseBodyResponse.code();
                       Log.d(TAG, "onNextResponseCode: "+code);

                       try
                       {
                           if(code == 200)
                           {
                               String streamId = new JSONObject(responseBodyResponse.body().string()).getJSONObject("data").getString("streamId");

                               Log.d(TAG, "onNextResponse: "+streamId);
                               view.onSuccess(streamId);

                           }

                       }catch (Exception e)
                       {
                           e.printStackTrace();
                       }
                   }

                   @Override
                   public void onError(Throwable e) {

                   }

                   @Override
                   public void onComplete() {

                   }
               });
    }


    @Override
    public void onChatRxJava() {

        Observer<Messages> observer = new Observer<Messages>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Messages messages) {

                Log.d(TAG, "onNextonChatRxJava: "+messages.getUserName());
                view.onMessageReceived(messages);
            }

            @Override
            public void onError(Throwable e) {

                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        };
        MyChatObservable.getInstance().subscribe(observer);
               /* .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);*/
    }

    @Override
    public void initializeRxJAva() {
        Observer<SubscribedData> observer = new Observer<SubscribedData>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(SubscribedData subscribedData) {

                view.onDataSubscribed(subscribedData);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };

        MySubscribeObservable.getInstance().subscribe(observer);


    }
}
