package com.livestreaming.newlivestream.signUpPage;

import com.livestreaming.newlivestream.utility.BasePresenter;
import com.livestreaming.newlivestream.utility.BaseViewImpl;

/**
 * Created by Ali on 11/21/2018.
 */
public interface SignUpActivityPresenterImpl
{
    interface SignUpPresenter extends BasePresenter
    {

        void validateFname(String s);

        boolean validatePhone(String isDialog, String trim, String trim1);

        boolean validateEmail(String isDialog, String s);

        void signUpApi(String countryCode, String name, String email, String phone);

    }
    interface SignUpView extends BaseViewImpl
    {

        void setMobileError(boolean isEmpty);

        void setMobileErrorMsg(String s);

        void setEmailError(boolean isEmpty);

        void setEmailValid(String s);

        void setFirstNameError();

        void clearError(int i);

        void moveToMainActivity();
    }
}
