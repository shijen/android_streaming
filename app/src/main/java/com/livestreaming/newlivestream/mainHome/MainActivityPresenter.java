package com.livestreaming.newlivestream.mainHome;

import android.util.Log;

import com.google.gson.Gson;
import com.livestreaming.newlivestream.ResponcePojo.LiveBroadCaster;
import com.livestreaming.newlivestream.dagger.ApiServices;
import com.livestreaming.newlivestream.network.NetworkStateHolder;
import com.livestreaming.newlivestream.network.RxNetworkObserver;
import com.livestreaming.newlivestream.utility.SessionManagerImpl;
import com.livestreaming.newlivestream.utility.VariableConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.livestreaming.newlivestream.utility.VariableConstant.IS_APP_BACKGROUND;
import static com.livestreaming.newlivestream.utility.VariableConstant.NETWORK_SCREEN_OPEN;

/**
 * Created by Ali on 11/20/2018.
 */
public class MainActivityPresenter implements MainActivityPresenterImpl.presenterMain {

    @Inject  MainActivityPresenterImpl.ViewMain viewMain;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    RxNetworkObserver rxNetworkObserver;

    @Inject
    SessionManagerImpl manager;
    @Inject
    ApiServices apiServices;
    @Inject Gson gson;

    @Inject
    public MainActivityPresenter() {
    }

    @Override
    public void checkForNetwork(boolean isConnected) {

        networkStateHolder.setConnected(isConnected);
        rxNetworkObserver.publishData(networkStateHolder);
        if(!isConnected && !IS_APP_BACKGROUND && !NETWORK_SCREEN_OPEN)
            viewMain.openNetworkScreen();

    }

    @Override
    public void callLiveBroadcaster() {

        Observable<Response<ResponseBody>>observable = apiServices.getLiveStreamers(manager.getAUTH(), VariableConstant.SEL_LANG);

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        int code = responseBodyResponse.code();
                        Log.d("TAG", "onNext: "+code);
                        try {
                            if(code == 200)
                            {

                                String response = responseBodyResponse.body().string();
                                Log.d("TAG", "onNextAvali: "+response);
                                LiveBroadCaster liveBroadCaster = gson.fromJson(response, LiveBroadCaster.class);

                                viewMain.liveBroadCasterData(liveBroadCaster.getData().getStreams());

                            }else
                            {
                                String responseError = responseBodyResponse.errorBody().string();
                                viewMain.showAlert(new JSONObject(responseError).getString("message"));
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
