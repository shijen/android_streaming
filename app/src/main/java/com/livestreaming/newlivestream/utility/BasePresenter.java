package com.livestreaming.newlivestream.utility;

/**
 * Created by Ali on 11/21/2018.
 */
public interface BasePresenter
{
    void connectView();
    void detachView();
}
