package com.livestreaming.newlivestream.loginPage;



import android.util.Log;

import com.google.gson.Gson;
import com.livestreaming.newlivestream.ResponcePojo.SignUpLoginResponse;
import com.livestreaming.newlivestream.dagger.ApiServices;
import com.livestreaming.newlivestream.utility.SessionManagerImpl;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.livestreaming.newlivestream.utility.VariableConstant.SEL_LANG;
import static com.livestreaming.newlivestream.utility.VariableConstant.SUCCESS;

/**
 * Created by Ali on 11/21/2018.
 */
public class LoginPresenterImpl implements LoginPresenter.LogInPresent {

    private final String TAG = LoginPresenterImpl.class.getSimpleName();

    @Inject
    LoginPresenter.LogInView logInView;

    @Inject
    ApiServices apiServices;

    @Inject
    Gson gson;

    @Inject
    SessionManagerImpl manager;



    @Inject
    public LoginPresenterImpl() {
    }


    @Override
    public void connectView() {

    }

    @Override
    public void detachView() {

    }

    @Override
    public void callSignInApi(String email)
    {
        Observable<Response<ResponseBody>> observable = apiServices.signIn(SEL_LANG,email,"+91",1);

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {

                        int code = responseBodyResponse.code();

                        Log.d(TAG, "onNext: "+code);
                        String response;
                        SignUpLoginResponse signUpLoginResponse;
                        logInView.hideProgress();
                        try {
                            switch (code)
                            {
                                case SUCCESS:
                                    response = responseBodyResponse.body().string();
                                    signUpLoginResponse = gson.fromJson(response,SignUpLoginResponse.class);
                                    manager.setFcmTopic(signUpLoginResponse.getData().getFcmTopic());
                                    manager.setAUTH(signUpLoginResponse.getData().getAuthToken());
                                    manager.setSID(signUpLoginResponse.getData().getId());
                                    manager.setEmail(signUpLoginResponse.getData().getEmail());
                                    manager.setCountryCode(signUpLoginResponse.getData().getPhone().getCountryCode());
                                    manager.setMobileNo(signUpLoginResponse.getData().getPhone().getPhone());
                                    manager.setFirstName(signUpLoginResponse.getData().getFirstName());
                                    logInView.moveToMainActivity();
                                    break;
                                    default:
                                        response = responseBodyResponse.errorBody().string();
                                        signUpLoginResponse = gson.fromJson(response,SignUpLoginResponse.class);
                                        logInView.apiError(signUpLoginResponse.getMessage());
                            }
                        }catch (Exception e)
                        {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        logInView.hideProgress();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
