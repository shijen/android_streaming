package com.livestreaming.newlivestream.mainHome;

import com.livestreaming.newlivestream.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * Created by Ali on 12/17/2018.
 */
@Module
public interface PreviewModule
{
    @Binds
    @ActivityScoped
    PreviewInterFace.PreViewPresenter providePreviewPresenter(PreviewPresenter previewPresenter);

    @Binds
    @ActivityScoped
    PreviewInterFace.PreviewView providePreviewView(PreviewActivity previewActivity);
}
