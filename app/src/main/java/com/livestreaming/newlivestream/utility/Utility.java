package com.livestreaming.newlivestream.utility;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by Ali on 12/21/2018.
 */
public class Utility
{
    public static void hideKeyboard(Activity mcontext) {
        try{
            InputMethodManager inputManager = (InputMethodManager) mcontext.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(mcontext.getCurrentFocus().getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
        }catch (NullPointerException e)
        {

        }

    }
    public static void showKeyBoard(Activity mContext)
    {
        try {
            InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
        }catch (Exception e)
        {

        }
    }

    public static long currentTIme()
    {
        Calendar calendar = Calendar.getInstance();
        /*if(addedTime>0)
            calendar.setTimeInMillis(addedTime*1000L);*/
        calendar.setTimeZone(getTimeZone());
        Log.d("TAG", "currentTIme: "+getTimeZone().getOffset(calendar.getTimeInMillis())+" rowOffset "+getTimeZone().getRawOffset());

        Timestamp timestamp = new Timestamp(calendar.getTimeInMillis());
        long time = (timestamp.getTime()/1000)+((getTimeZone().getRawOffset())/1000);
        // long time = calendar.getTimeInMillis()/1000;

        // timestamp.getTime();

        return time;
    }

    public static TimeZone getTimeZone()
    {
        String timeZoneString =  TimezoneMapper.latLngToTimezoneString(12.9716,
                77.5946);
        return TimeZone.getTimeZone(timeZoneString);
    }

}
