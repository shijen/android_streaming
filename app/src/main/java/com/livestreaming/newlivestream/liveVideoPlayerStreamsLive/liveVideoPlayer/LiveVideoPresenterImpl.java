package com.livestreaming.newlivestream.liveVideoPlayerStreamsLive.liveVideoPlayer;

import com.livestreaming.newlivestream.utility.BasePresenter;
import com.livestreaming.newlivestream.utility.BaseViewImpl;

/**
 * Created by Ali on 12/21/2018.
 */
public interface LiveVideoPresenterImpl
{
    interface LivePresenter extends BasePresenter
    {

        void onLiveChat(String streamId, String msg);

        void getLiveChatsOn(String streamId);

        void subscribeToApi(String streamId, String action);
    }
    interface LiveView extends BaseViewImpl
    {

        void onStopSubscribe();

    }
}
