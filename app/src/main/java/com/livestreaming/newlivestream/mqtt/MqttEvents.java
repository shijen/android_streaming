package com.livestreaming.newlivestream.mqtt;

/**
 * <h>MqttEvents</h>
 * Created by ALi on 27/09/17.
 */


public enum MqttEvents {


   /* Provider("provider"),
    JobStatus("jobStatus"),
    LiveTrack("liveTrack"),
    Message("message"),*/
    PresenceTopic("PresenceTopic"),
    WillTopic("stream/lastWill"),
    StreamNow("stream-now"),
    StreamChat("stream-chat"),
    StreamSubscrib("stream-subscribe");
    public String value;

    MqttEvents(String value) {
        this.value = value;
    }


}