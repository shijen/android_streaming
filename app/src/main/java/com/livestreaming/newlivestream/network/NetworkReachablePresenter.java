package com.livestreaming.newlivestream.network;


import android.util.Log;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

import static com.livestreaming.newlivestream.utility.VariableConstant.NETWORK;

public class NetworkReachablePresenter implements NetworkReachableContract.Presenter
{
    private static final String TAG = NetworkReachablePresenter.class.getSimpleName();

    @Inject NetworkReachableContract.View view;
    @Inject RxNetworkObserver rxNetworkObserver;
    @Inject @Named(NETWORK) CompositeDisposable compositeDisposable;

    private Disposable networkDisposable;

    @Inject
    NetworkReachablePresenter() { }

    @Override
    public void subscribeNetworkObserver()
    {
        rxNetworkObserver.subscribeOn(Schedulers.io());
        rxNetworkObserver.observeOn(AndroidSchedulers.mainThread());
        networkDisposable = rxNetworkObserver.subscribeWith( new DisposableObserver<NetworkStateHolder>()
        {
            @Override
            public void onNext(NetworkStateHolder networkStateHolder)
            {

                Log.d(TAG, "network not available: "+networkStateHolder.isConnected());
                if(networkStateHolder.isConnected())
                    view.networkAvailable();
                else
                    view.networkNotAvailable();
            }

            @Override
            public void onError(Throwable e)
            {
                e.printStackTrace();
            }

            @Override
            public void onComplete()
            {
                compositeDisposable.add(networkDisposable);
            }
        });
    }

    @Override
    public void disposeObservable() {
        compositeDisposable.dispose();
    }
}
