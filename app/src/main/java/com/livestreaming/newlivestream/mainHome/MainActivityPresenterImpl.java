package com.livestreaming.newlivestream.mainHome;

import com.livestreaming.newlivestream.ResponcePojo.DataStreams;

import java.util.ArrayList;

/**
 * Created by Ali on 11/20/2018.
 */
public interface MainActivityPresenterImpl
{
    interface  presenterMain
    {

        void checkForNetwork(boolean isConnected);

        void callLiveBroadcaster();

    }
    interface ViewMain
    {

        void openNetworkScreen();

        void showAlert(String message);

        void liveBroadCasterData(ArrayList<DataStreams> streams);
    }
}
