package com.livestreaming.newlivestream.loginPage;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.livestreaming.newlivestream.R;
import com.livestreaming.newlivestream.mainHome.MainActivity;
import com.livestreaming.newlivestream.utility.AlertProgress;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerAppCompatActivity;

public class LonginActivity extends DaggerAppCompatActivity implements LoginPresenter.LogInView {

    @BindView(R.id.toolBar)Toolbar toolBar;
    @BindView(R.id.tv_center)TextView tv_center;
    @BindView(R.id.txtInputLayoutPhone)TextInputLayout txtInputLayoutPhone;
    @BindView(R.id.etEmailPh)EditText etEmailPh;
    @BindView(R.id.tvDoLogin)TextView tvDoLogin;
    @Inject AlertProgress alertProgress;
    @Inject LoginPresenter.LogInPresent present;
    private AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_longin);
        ButterKnife.bind(this);
        toolBar();
    }

    private void toolBar() {
        tv_center.setText(getString(R.string.splash_login));
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    @OnClick({R.id.tvDoLogin})
    public void onClickView(View v)
    {
        switch (v.getId())
        {
            case R.id.tvDoLogin:

                if(!"".equals(etEmailPh.getText().toString()))
                {
                    callApi();
                }else
                    alertProgress.alertinfo(this,getString(R.string.emailIsEmpty));

                break;
        }
    }

    private void callApi() {
        showProgress();
        present.callSignInApi(etEmailPh.getText().toString());

    }

    @Override
    public void showProgress() {
        dialog = alertProgress.getProgressDialog(this,getString(R.string.loggingin));
        dialog.show();
    }

    @Override
    public void hideProgress() {

        if(dialog!=null)
            dialog.dismiss();
    }

    @Override
    public void moveToMainActivity() {
        Intent intent = new Intent(this,MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void apiError(String errorMsg) {

        alertProgress.alertinfo(this,errorMsg);
    }

    @Override
    public void onNetworkRetry(boolean isNetwork, String thisApi) {

    }
}
