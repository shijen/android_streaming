package com.livestreaming.newlivestream.liveVideoPlayerStreamsLive.liveVideoPlayer;

import android.util.Log;

import com.google.gson.Gson;
import com.livestreaming.newlivestream.dagger.ApiServices;
import com.livestreaming.newlivestream.utility.SessionManagerImpl;
import com.livestreaming.newlivestream.utility.VariableConstant;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by Ali on 12/21/2018.
 */
public class LiveVideoPlayerPresenter implements LiveVideoPresenterImpl.LivePresenter
{

    @Inject
    ApiServices apiServices;
    @Inject
    Gson gson;
    @Inject
    SessionManagerImpl manager;

    @Inject LiveVideoPresenterImpl.LiveView view;

    @Inject
    public LiveVideoPlayerPresenter() {
    }

    @Override
    public void onLiveChat(String streamId, String msg)
    {
        Observable<Response<ResponseBody>> observer = apiServices.postApiChatStream(manager.getAUTH(), VariableConstant.SEL_LANG
        ,streamId,msg);

        observer.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {

                        int code = responseBodyResponse.code();
                        Log.d("onLiveChat", "onNext: "+code);
                        try
                        {
                            String response;
                            if(code == 200)
                            {
                                response = responseBodyResponse.body().string();
                                Log.d("onLiveChat", "onNextSUCC: "+response);
                            }else
                            {
                                response = responseBodyResponse.errorBody().string();
                                Log.d("onLiveChat", "onNextERR: "+response);
                            }


                        }catch (Exception e)

                        {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    @Override
    public void getLiveChatsOn(String streamId) {


        Observable<Response<ResponseBody>>observable = apiServices.getApiChat(manager.getAUTH(),VariableConstant.SEL_LANG,
                streamId,(System.currentTimeMillis()/1000));

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        int code = responseBodyResponse.code();
                        Log.d("getLiveChatsOn", "onNext: "+code);
                        String response;
                        try{
                            if(code==200)
                            {
                                response = responseBodyResponse.body().string();
                                Log.d("getLiveChatsOn", "onNextSUCC: "+response);
                            }else
                            {
                                response = responseBodyResponse.errorBody().string();
                                Log.d("getLiveChatsOn", "onNextERR: "+response);
                            }
                        }catch (Exception e)
                        {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void subscribeToApi(String streamId, String action) {
        Log.d("TAG", "subscribeToApi: "+streamId
        +" session "+manager.getAUTH());
        Observable<Response<ResponseBody>> observable = apiServices.postApiSubscribe(manager.getAUTH(),VariableConstant.SEL_LANG,
                streamId,action);

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {

                        int code = responseBodyResponse.code();
                        Log.d("subscribeToApi", "onNext: "+responseBodyResponse);
                        try
                        {
                            String response;
                            if(code == 200)
                            {
                                response = responseBodyResponse.body().string();
                                Log.d("subscribeToApi", "onNextSUCC: "+response);
                                if(action.equals("stop"))
                                {
                                    view.onStopSubscribe();
                                }
                            }else
                            {
                                response = responseBodyResponse.errorBody().string();
                                Log.d("subscribeToApi", "onNextERR: "+response);
                            }
                        }catch (Exception e)
                        {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void connectView() {

    }

    @Override
    public void detachView() {

    }
}
