package com.livestreaming.newlivestream.liveVideoPlayerStreamsLive.liveVideoPlayer;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlaybackControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultAllocator;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.livestreaming.newlivestream.R;
import com.livestreaming.newlivestream.ResponcePojo.DataStreams;
import com.livestreaming.newlivestream.ResponcePojo.Messages;
import com.livestreaming.newlivestream.ResponcePojo.MyChatObservable;
import com.livestreaming.newlivestream.ResponcePojo.MyStreamObservable;
import com.livestreaming.newlivestream.mainHome.MessageAdapter;
import com.livestreaming.newlivestream.mqtt.MQTTManager;
import com.livestreaming.newlivestream.mqtt.MqttEvents;
import com.livestreaming.newlivestream.utility.SessionManagerImpl;
import com.livestreaming.newlivestream.utility.Utility;

import java.io.IOException;
import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerAppCompatActivity;
import io.reactivex.Observer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

import static com.google.android.exoplayer2.C.DEFAULT_BUFFER_SEGMENT_SIZE;
import static com.livestreaming.newlivestream.mainHome.MainActivity.RTMP_BASE_URL;


/**
 * Created by Ali on 11/28/2018.
 */
public class LiveVideoPlayer extends DaggerAppCompatActivity implements LiveVideoPresenterImpl.LiveView
{
    private PlayerView player_view;
    private SimpleExoPlayer player;
    private String rtmpUrl = RTMP_BASE_URL;
    private String streamName;
    private String streamId;
    private DefaultTrackSelector trackSelector;
    private BandwidthMeter BANDWIDTH_METER = new DefaultBandwidthMeter();
    private com.google.android.exoplayer2.source.MediaSource mediaSource;
    private RtmpDataSource.RtmpDataSourceFactory rtmpDataSourceFactory;
    private Uri uri;
    private Handler mainHandler;
    private ExtractorMediaSource.EventListener eventLogger;

    private static final String TAG = "MainActivity";

    @Inject
    MQTTManager mqttManager;
    @Inject
    SessionManagerImpl manager;
    @Inject LiveVideoPresenterImpl.LivePresenter presenter;

    @BindView(R.id.sendMessage)ImageView sendMessage;
    @BindView(R.id.etSendYourText)EditText etSendYourText;
    @BindView(R.id.recyclerViewMesg)RecyclerView recyclerViewMesg;
    @BindView(R.id.ivCloseVideo)ImageView ivCloseVideo;
    private Observer<DataStreams>observer;
    private CompositeDisposable disposable;
    private String action;
    private MessageAdapter messageAdapter;
    private ArrayList<Messages> messages = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.live_video_play);
        ButterKnife.bind(this);
        disposable = new CompositeDisposable();

        streamName = getIntent().getStringExtra("streamName");
        streamId = getIntent().getStringExtra("streamId");
        rtmpUrl = rtmpUrl +streamName;
        player_view = findViewById(R.id.player_view);
        player_view.setControllerVisibilityListener(new PlaybackControlView.VisibilityListener() {
            @Override public void onVisibilityChange(int visibility) {
                if (visibility == View.VISIBLE){
                    Log.d(TAG, "onVisibilityChange: is visible");
                }
            }
        });
        player_view.requestFocus();

        rtmpDataSourceFactory = new RtmpDataSource.RtmpDataSourceFactory();
        uri = Uri.parse(rtmpUrl);
        mainHandler = new Handler();
        eventLogger = new ExtractorMediaSource.EventListener() {
            @Override public void onLoadError(IOException error) {
                Log.e(TAG, "onLoadError: ", error);
            }
        };


        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

        DefaultAllocator defaultAllocator = new DefaultAllocator(true, DEFAULT_BUFFER_SEGMENT_SIZE);
        // DefaultLoadControl loadControl = new DefaultLoadControl(defaultAllocator, 1000, 3000, 1000, 1000, -1, true);

        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
        player_view.setPlayer(player);
        player_view.setUseController(false);

        //  RtmpDataSource.RtmpDataSourceFactory rtmpDataSourceFactory = new RtmpDataSource.RtmpDataSourceFactory();
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

        ExtractorMediaSource mediaSource = new ExtractorMediaSource
                .Factory(rtmpDataSourceFactory)
                .setExtractorsFactory(extractorsFactory)
                .createMediaSource(Uri.parse(RTMP_BASE_URL+streamName+" live=1 buffer=1000"));


        player.prepare(mediaSource);
        player.setPlayWhenReady(true);

        getChats();
        createMqtt();
        action = "start";
        subscribeToApi();
        InitializeAdapter();
    }

    private void InitializeAdapter() {
        linearLayoutManager = new LinearLayoutManager(this);
        messageAdapter = new MessageAdapter(messages,this);
        recyclerViewMesg.setLayoutManager(linearLayoutManager);
        recyclerViewMesg.setAdapter(messageAdapter);
    }


    private void subscribeToApi() {
        presenter.subscribeToApi(streamId,action);
    }

    private void getChats() {

        presenter.getLiveChatsOn(streamId);
    }

    @OnClick ({R.id.ivCloseVideo,R.id.sendMessage})
    public void onViewClicked(View v)
    {
        switch (v.getId())
        {
            case R.id.ivCloseVideo:
                action = "stop";

                onBackPressed();
                break;
            case R.id.sendMessage:
                if(!etSendYourText.getText().toString().trim().equals(""))
                {
                    String msg = etSendYourText.getText().toString().trim();
                    presenter.onLiveChat(streamId,msg);
                    etSendYourText.setText("");
                    Utility.hideKeyboard(this);
                  /*  String name  = manager.getFirstName()+" "+manager.getLastName();
                    Messages message = new Messages(msg,manager.getSID(),name,"");
                    messages.add(message);
                    messageAdapter.notifyDataSetChanged();*/

                }
                break;
        }
    }

    private void createMqtt() {
        if(!mqttManager.isMQTTConnected())
        {
            mqttManager.createMQttConnection(manager.getSID());

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    subscribeToNoTopic();
                    subscribeToMessageTopic();
                    subscribeToSubscribe();
                }
            }, 2000);
        }else
        {
            subscribeToNoTopic();
            subscribeToMessageTopic();
            subscribeToSubscribe();
        }
        initializeRxJava();
        subscribeRxJava();
        chatRxJava();
    }

    private void subscribeRxJava() {

    }

    private void chatRxJava()
    {
       Observer<Messages> observer = new Observer<Messages>() {
           @Override
           public void onSubscribe(Disposable d) {

           }

           @Override
           public void onNext(Messages message) {

               messages.add(message);
               messageAdapter.notifyDataSetChanged();
               scrollToBottom();

           }

           @Override
           public void onError(Throwable e) {

               e.printStackTrace();
           }

           @Override
           public void onComplete() {

           }
       };
        MyChatObservable.getInstance().subscribe(observer);


    }
    private void scrollToBottom() {
        recyclerViewMesg.scrollToPosition(messageAdapter.getItemCount() - 1);
    }

    private void subscribeToSubscribe() {
        mqttManager.subscribeToTopic(MqttEvents.StreamNow.value + "/" , 1);
    }

    private void initializeRxJava()
    {
        observer = new Observer<DataStreams>() {
            @Override
            public void onSubscribe(Disposable d) {

                disposable.dispose();
                disposable.clear();
                disposable.add(d);
            }

            @Override
            public void onNext(DataStreams dataStream) {

                Log.d("TAG", "onNextRXOBSERVER: "+dataStream.getAction()+" streamId "+
                dataStream.getStreamId());
                if(dataStream!=null)
                {
                    if(dataStream.getAction().equalsIgnoreCase("stop"))
                    {
                        onBackPressed();
                    }
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        };
        MyStreamObservable.getInstance().subscribe(observer);
    }

    private void subscribeToMessageTopic() {
        mqttManager.subscribeToTopic(MqttEvents.StreamChat.value+"/"+streamId,1);
    }

    private void subscribeToNoTopic() {
        mqttManager.subscribeToTopic(MqttEvents.StreamSubscrib.value+"/"+streamId,1);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(observer!=null)
            MyStreamObservable.getInstance().removeObserver(observer);
        disposable.clear();
        disposable.dispose();

        if(mqttManager.isMQTTConnected())
        {
            mqttManager.unSubscribeToTopic(MqttEvents.StreamSubscrib.value+"/"+streamId);
            mqttManager.unSubscribeToTopic(MqttEvents.StreamChat.value+"/"+streamId);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        presenter.subscribeToApi(streamId,action);

    }


    @Override
    public void onStopSubscribe() {
        if(observer!=null)
            MyStreamObservable.getInstance().removeObserver(observer);
        observer = null;
        disposable.clear();
        disposable.dispose();
        finish();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void apiError(String errorMsg) {

    }

    @Override
    public void onNetworkRetry(boolean isNetwork, String thisApi) {

    }
}

