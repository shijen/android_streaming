package com.livestreaming.newlivestream.mainHome;

import android.util.Log;

import com.livestreaming.newlivestream.dagger.ApiServices;
import com.livestreaming.newlivestream.liveVideoPlayerStreamsLive.liveVideoBroadcaster.LiveBroadCastPresenter;
import com.livestreaming.newlivestream.utility.SessionManagerImpl;
import com.livestreaming.newlivestream.utility.VariableConstant;

import org.json.JSONObject;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by Ali on 12/17/2018.
 */
public class PreviewPresenter implements PreviewInterFace.PreViewPresenter
{

    private static final String TAG = LiveBroadCastPresenter.class.getSimpleName();
    @Inject
    ApiServices apiServices;
    @Inject
    SessionManagerImpl manager;

    @Inject PreviewInterFace.PreviewView view;

    @Inject
    public PreviewPresenter() {
    }

    @Override
    public void onLiveBroadCastApi(String streamId, String streamType, String thumbnail, String streamName) {
        Observable<Response<ResponseBody>> observable = apiServices.postApiCallStream(manager.getAUTH(), VariableConstant.SEL_LANG
                ,streamId,streamType,streamName,thumbnail,false,false);

        Log.d(TAG, "onLiveBroadCastApi: "+streamId+" streamType "+streamType
        +" nail "+thumbnail+" name "+streamName +" auth\n"+manager.getAUTH());
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {


                        int code = responseBodyResponse.code();
                        Log.d(TAG, "onNextResponseCode: "+code);

                        try
                        {
                            if(code == 200)
                            {
                                String resp = responseBodyResponse.body().string();
                                Log.d(TAG, "onNextResponse: "+resp);
                                String streamId = new JSONObject(resp).getJSONObject("data").getString("streamId");

                                Log.d(TAG, "onNextResponse: "+streamId);
                                view.onSuccess(streamId);

                            }

                        }catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
