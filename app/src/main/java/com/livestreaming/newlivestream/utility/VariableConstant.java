package com.livestreaming.newlivestream.utility;

/**
 * Created by Ali on 11/20/2018.
 */
public class VariableConstant
{
    public static final String BASE_URL = " http://206.189.23.214:5656/" ;

    public static final String MQTT_URL_HOST = "tcp://142.93.42.217";
    public static final String MQTT_PORT = "2052";

    public static boolean  IS_APP_BACKGROUND = false;
    public static boolean  NETWORK_SCREEN_OPEN = false;
    public static final String SEL_LANG = "en";
    public static final int SUCCESS = 200;
    public static final String NETWORK = "Network";

    public static final String Amazonbucket = "appscrip";
    public static final String Amazoncognitoid = "us-east-1:9a14c311-0b08-4a80-a725-157af6547833";
    public static final String AmazonThumb = "LiveStream/thumbNail";
}
