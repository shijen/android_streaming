package com.livestreaming.newlivestream.dagger;

import android.content.Context;

import com.google.gson.Gson;
import com.livestreaming.newlivestream.countrypic.CountryPicker;
import com.livestreaming.newlivestream.mqtt.MQTTManager;
import com.livestreaming.newlivestream.network.NetworkStateHolder;
import com.livestreaming.newlivestream.network.RxNetworkObserver;
import com.livestreaming.newlivestream.utility.AlertProgress;
import com.livestreaming.newlivestream.utility.AppTypeface;
import com.livestreaming.newlivestream.utility.SessionManagerImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * @author Pramod
 * @since 05-01-2018
 */
@Module
public class UtilsModule {

    @Provides
    @Singleton
    CompositeDisposable provideDisposable(){
        return new CompositeDisposable();
    }


   /* @Provides
    @Singleton
    ProviderObservable provideObservable(){return new ProviderObservable();}

    @Named("Booking")
    @Provides
    @Singleton
    MyBookingObservable provideMyBookngObservable(){return new MyBookingObservable();}*/

    @Provides
    @Singleton
    RxNetworkObserver provideRxNetwork()
    {
        return new RxNetworkObserver();
    }

    //, ProviderObservable rxProviderObserver,
    @Provides
    @Singleton
    MQTTManager mqttManager(Context context,
                            SessionManagerImpl sessionManagerImpl, Gson gson)//,MyBookingObservable rxMyBookingObservable
    {
        return new MQTTManager(context,sessionManagerImpl,gson);//rxProviderObserver,
    }


    @Provides
    @Singleton
    Gson provideGSON(){return new Gson();}

    @Provides
    @Singleton
    NetworkStateHolder provideNetworkStateHolder(){return new NetworkStateHolder();}

    @Provides
    @Singleton
    AppTypeface provideAppTypeFace(Context mContext){return AppTypeface.getInstance(mContext);}


    @Provides
    @Singleton
    AlertProgress provideAlertProgress(Context mContext) {return new AlertProgress(mContext);}

    @Provides
    @Singleton
    CountryPicker countryPicker() { return new CountryPicker();}



}
