package com.livestreaming.newlivestream.signUpPage;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.livestreaming.newlivestream.R;
import com.livestreaming.newlivestream.countrypic.Country;
import com.livestreaming.newlivestream.countrypic.CountryPicker;
import com.livestreaming.newlivestream.mainHome.MainActivity;
import com.livestreaming.newlivestream.utility.AlertProgress;
import com.livestreaming.newlivestream.utility.DialogInterfaceListner;

import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import dagger.android.support.DaggerAppCompatActivity;

public class SignUpActivity extends DaggerAppCompatActivity implements SignUpActivityPresenterImpl.SignUpView
{


    @BindView(R.id.toolBar)Toolbar toolBar;
    @BindView(R.id.tv_center)TextView tv_center;

    @BindView(R.id.txtInputLayoutName)TextInputLayout txtInputLayoutName;
    @BindView(R.id.etName)EditText etName;
    @BindView(R.id.txtInputLayoutEmail)TextInputLayout txtInputLayoutEmail;
    @BindView(R.id.etEmail)EditText etEmail;
    @BindView(R.id.txtInputLayoutPhone)TextInputLayout txtInputLayoutPhone;
    @BindView(R.id.etPhoneNumber)EditText etPhoneNumber;
    @BindView(R.id.countryFlag)ImageView countryFlag;
    @BindView(R.id.countryCode)TextView countryCode;
    @BindView(R.id.tvError)TextView tvError;

    @BindView(R.id.tvDoSignUp)TextView tvDoSignUp;

    @Inject
    CountryPicker mCountryPicker;
    @Inject SignUpActivityPresenterImpl.SignUpPresenter presenter;

    @Inject
    AlertProgress alertProgress;
    private AlertDialog dialog;


    View focusView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        toolBar();
        setListener();
    }


    private void toolBar() {
        tv_center.setText(getString(R.string.splash_signup));
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolBar.setNavigationOnClickListener(view -> onBackPressed());

    }

    @OnClick({R.id.countryCode,R.id.countryFlag,R.id.tvDoSignUp})
    public void onClickView(View v)
    {
        switch (v.getId())
        {
            case R.id.countryCode:
            case R.id.countryFlag:
                mCountryPicker.show(getSupportFragmentManager(), getResources().getString(R.string.Countrypicker));
                break;
            case R.id.tvDoSignUp:
                if(focusView==null)
                    signUpApi();
                else
                    focusView.requestFocus();
                break;
        }
    }

    private void signUpApi() {
        showProgress();
        presenter.signUpApi(countryCode.getText().toString(),etName.getText().toString(),etEmail.getText().toString().trim(),etPhoneNumber.getText().toString().trim());
    }

    @OnFocusChange({R.id.etName,R.id.etEmail,R.id.etPhoneNumber})
    public void onFocusChangedView(View v, boolean hasFocus)
    {
        switch (v.getId())
        {
            case R.id.etName:
                if(!hasFocus) {
                    focusViewNull();
                    //Check First name
                    presenter.validateFname(etName.getText().toString());
                }
                break;
            case R.id.etEmail:
                if(!hasFocus) {
                    focusViewNull();
                    //Checks email
                    presenter.validateEmail("y",etEmail.getText().toString().toLowerCase(Locale.getDefault()));
                }
                break;

            case R.id.etPhoneNumber:
                focusViewNull();
                if(!hasFocus) {
                    presenter.validatePhone("y", countryCode.getText().toString().trim(),etPhoneNumber.getText().toString().trim());
                }
                break;
        }
    }

    @Override
    public void setMobileError(boolean isEmpty) {

        if(isEmpty)
        {
            tvError.setText(getString(R.string.PhoneEmpty));
            tvError.setVisibility(View.VISIBLE);
            setFocusView(txtInputLayoutPhone);
        }else
        {
            tvError.setText(getString(R.string.PhoneInvalid));
            tvError.setVisibility(View.VISIBLE);
            setFocusView(txtInputLayoutPhone);
        }
    }

    @Override
    public void setMobileErrorMsg(String s) {
        tvError.setText("");
        tvError.setVisibility(View.GONE);
        focusViewNull();
    }

    @Override
    public void setEmailError(boolean isEmpty) {
        if(isEmpty)
        {
            txtInputLayoutEmail.setErrorEnabled(true);
            txtInputLayoutEmail.setError(getString(R.string.emailIsEmpty));
            setFocusView(txtInputLayoutEmail);
        }else
        {
            txtInputLayoutEmail.setErrorEnabled(true);
            txtInputLayoutEmail.setError(getString(R.string.emailInvalid));
            setFocusView(txtInputLayoutEmail);

        }
    }

    @Override
    public void setEmailValid(String s) {
        focusViewNull();
        txtInputLayoutEmail.setErrorEnabled(false);
        txtInputLayoutEmail.setError("");

    }

    @Override
    public void setFirstNameError() {
        setFocusView(txtInputLayoutName);
        txtInputLayoutName.setErrorEnabled(true);
        txtInputLayoutName.setError(getString(R.string.nameIsEmpty));
    }

    @Override
    public void clearError(int i) {

    }

    private void setFocusView(TextInputLayout txtInputLayoutPhone) {
        focusView = txtInputLayoutPhone;
    }

    private void focusViewNull() {
        focusView = null;
    }

    @Override
    public void showProgress() {
        dialog = alertProgress.getProgressDialog(this,getString(R.string.registering));
        dialog.show();

    }

    @Override
    public void hideProgress() {

        if(dialog!=null)
            dialog.dismiss();
    }

    @Override
    public void apiError(String errorMsg) {
        alertProgress.alertinfo(this,errorMsg);
    }

    @Override
    public void onNetworkRetry(boolean isNetwork, String thisApi) {

        alertProgress.tryAgain(this, getString(R.string.checkYourInternet), getString(R.string.alert), new DialogInterfaceListner() {
            @Override
            public void dialogClick(boolean isClicked) {
                if(isClicked)
                {
                    if(thisApi.equals("signUp"))
                      signUpApi();

                }
            }
        });
    }

    @Override
    public void moveToMainActivity() {
        Intent intent = new Intent(this,MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    /**
     * <h2>setListener</h2>
     *    <p>This is to set listener for CountryPicker to get all the countryCodes,Flags and map them accordingly</p>
     */
    private void setListener() {
        mCountryPicker.setListener((name, code, dialCode, flagDrawableResID, max) -> {
            countryCode.setText(dialCode);
            countryFlag.setImageResource(flagDrawableResID);
            mCountryPicker.dismiss();
           // et_reg_mobno.setFilters(new InputFilter[]{new InputFilter.LengthFilter(max)});

        });
        countryCode.setOnClickListener(v -> mCountryPicker.show(getSupportFragmentManager(), getResources().getString(R.string.Countrypicker)));
        //By Default, Current country
        Country country = mCountryPicker.getUserCountryInfo(this);
        countryFlag.setImageResource(country.getFlag());
        countryCode.setText(country.getDial_code());
    }
}
