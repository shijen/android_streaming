package com.livestreaming.newlivestream;

import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.livestreaming.newlivestream.mqtt.MQTTManager;
import com.livestreaming.newlivestream.utility.SessionManagerImpl;

import javax.inject.Inject;

import dagger.android.DaggerService;

/**
 * Created by Ali on 1/20/2018.
 */

public class OnMyService extends DaggerService
{
    //  SessionManager sharedPrefs;

    @Inject
    SessionManagerImpl sharedPrefs;
    @Inject
    MQTTManager manager;
    @Inject
    public OnMyService() {

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        //sharedPrefs = SessionManager.getInstance(this);
        Log.e("ClearFromRecentService", "Service Started");
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("ClearFromRecentService", "Service Destroyed");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        String cid = sharedPrefs.getSID();
        Log.e("ClearFromRecentService", "END "+cid+" MQTTRESPO "+manager.isMQTTConnected());
        //Code here

        if(manager.isMQTTConnected())
        {
          /*  if(!cid.equals(""))
            {
                manager.unSubscribeToTopic(MqttEvents.JobStatus.value + "/" +cid);
                manager.unSubscribeToTopic(MqttEvents.Provider.value + "/" +cid);
                manager.unSubscribeToTopic(MqttEvents.Message.value + "/" + cid);


            }*/
            manager.disconnect(cid);
        }

        stopSelf();
    }

}
