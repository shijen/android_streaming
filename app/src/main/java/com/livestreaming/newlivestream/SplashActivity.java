package com.livestreaming.newlivestream;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.livestreaming.newlivestream.loginPage.LonginActivity;
import com.livestreaming.newlivestream.mainHome.MainActivity;
import com.livestreaming.newlivestream.signUpPage.SignUpActivity;
import com.livestreaming.newlivestream.utility.SessionManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SplashActivity extends AppCompatActivity {


    @BindView(R.id.tvLogin) TextView tvLogin;
    @BindView(R.id.tvSignUp) TextView tvSignUp;
    @BindView(R.id.llSplashLoinSinupBt) LinearLayout llSplashLoinSinupBt;
    SessionManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        manager = SessionManager.getInstance(this);
        checkVisibility();
    }

    private void checkVisibility() {

        Log.d("TAG", "checkVisibility: "+manager.getAUTH());
        if(!"".equals(manager.getAUTH()))
        {
            Intent intent = new Intent(this, MainActivity.class);
           // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
            //  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }else
        {
            llSplashLoinSinupBt.setVisibility(View.VISIBLE);
        }
    }

    @OnClick({R.id.tvLogin,R.id.tvSignUp})
    public void onClickView(View v)
    {
        Intent intent;
        switch (v.getId())
        {
            case R.id.tvLogin:
                tvLogin.setSelected(false);
                tvSignUp.setSelected(false);
                intent = new Intent(this,LonginActivity.class);
                startActivity(intent);
                break;
            case R.id.tvSignUp:
                tvLogin.setSelected(true);
                tvSignUp.setSelected(true);
                intent = new Intent(this,SignUpActivity.class);
                startActivity(intent);
                break;
        }
    }
}
