package com.livestreaming.newlivestream.utility;


/**
 * <h>SessionManagerImpl</h>
 * Created by Ali on 1/30/2018.
 */

public interface SessionManagerImpl
{

    void clearSession();

    void setSID(String sid);
    String getSID();

    void setAUTH(String auth);
    String getAUTH();

    void setLatitude(String latitude);
    String getLatitude();

    void setLongitude(String longitude);
    String getLongitude();

    void setAddress(String address);
    String getAddress();

    void setHomeScreenData(String Data);
    String getHomeScreenData();

    void setFirstName(String firstName);
    String getFirstName();

    void setLastName(String lastName);
    String getLastName();

    void setEmail(String email);
    String getEmail();

    void setMobileNo(String mobileNo);
    String getMobileNo();

    void setAbout(String about);
    String getAbout();

    void setRegisterId(String registerId);
    String getRegisterId();

    void setReferralCode(String refCode);
    String getReferralCode();

    void setGuestLogin(boolean guestLogin);
    boolean getGuestLogin();

    void setProfileCalled(boolean isProfileCalled);
    boolean isProfileCalled();

    String getProfilePicUrl();
    void setProfilePicUrl(String profilePicUrl);

    String getCountryCode();
    void setCountryCode(String countryCode);

    String getFcmTopic();
    void setFcmTopic(String fcmTopic);

}

