package com.livestreaming.newlivestream.dagger;


import com.livestreaming.newlivestream.OnMyService;
import com.livestreaming.newlivestream.liveVideoPlayerStreamsLive.liveVideoBroadcaster.LiveBroadCastModule;
import com.livestreaming.newlivestream.liveVideoPlayerStreamsLive.liveVideoBroadcaster.LiveVideoBroadcasterActivity;
import com.livestreaming.newlivestream.liveVideoPlayerStreamsLive.liveVideoPlayer.LiveVideoPlayer;
import com.livestreaming.newlivestream.liveVideoPlayerStreamsLive.liveVideoPlayer.LiveVideoPlayerModule;
import com.livestreaming.newlivestream.loginPage.LoginModule;
import com.livestreaming.newlivestream.loginPage.LonginActivity;
import com.livestreaming.newlivestream.mainHome.MainActivity;
import com.livestreaming.newlivestream.mainHome.MainActivityModule;
import com.livestreaming.newlivestream.mainHome.PreviewActivity;
import com.livestreaming.newlivestream.mainHome.PreviewModule;
import com.livestreaming.newlivestream.signUpPage.SignUpActivity;
import com.livestreaming.newlivestream.signUpPage.SignUpActivityModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * @author Pramod
 * @since 11/12/17.
 */

@Module
public interface ActivityBindingModule
{
    @ContributesAndroidInjector
    OnMyService service();


    @ActivityScoped
    @ContributesAndroidInjector(modules = LoginModule.class)
    LonginActivity loginActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = SignUpActivityModule.class)
    SignUpActivity sinUpActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = MainActivityModule.class)
    MainActivity mainActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = LiveBroadCastModule.class)
    LiveVideoBroadcasterActivity liveBroadCaterActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = PreviewModule.class)
    PreviewActivity previewActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = LiveVideoPlayerModule.class)
    LiveVideoPlayer LiveVideoPLayerActivity();


}
