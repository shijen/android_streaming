package com.livestreaming.newlivestream.network;

import com.livestreaming.newlivestream.dagger.ActivityScoped;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

import static com.livestreaming.newlivestream.utility.VariableConstant.NETWORK;


/**
 * <h1>AddCardDaggerUtilModule</h1>
 * used to provide models to dagger
 * @author 3Embed
 * @since on 21-03-2018.
 */
@Module
public class NetworkReachableUtilModule
{
    @Provides
    @ActivityScoped
    @Named(NETWORK)
    CompositeDisposable provideCompositeDisposable()
    {
        return new CompositeDisposable();
    }
}