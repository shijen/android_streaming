package com.livestreaming.newlivestream.dagger;

import java.util.ArrayList;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Ali on 11/20/2018.
 */
public interface ApiServices
{
    @FormUrlEncoded
    @POST("/customer/provider")
    Observable<Response<ResponseBody>> getProviders(@Header("authorization") String authorization,
                                                    @Header("lan") String selLang,
                                                    @Field("lat") double lat,
                                                    @Field("long") double longitude,
                                                    @Field("categoryId") String catId,
                                                    @Field("subCategoryId") String subCatId,
                                                    @Field("distance") double distance,
                                                    @Field("minPrice") double minPrice,
                                                    @Field("maxPrice") double maxPrice,
                                                    @Field("bookingType") int bookingType,
                                                    @Field("scheduleDate") String scheduleDate,
                                                    @Field("scheduleTime") int scheduleTime,
                                                    @Field("endTimeStamp") long endTimeStamp,
                                                    @Field("days") ArrayList<String> days,
                                                    @Field("deviceTime") String deviceTime,
                                                    @Field("ipAddress") String ipAddress);

    @FormUrlEncoded
    @POST("/signUp")
    Observable<Response<ResponseBody>> signUpApi(@Header("lan") String selLang,
                                                 @Field("firstName") String fName,
                                                 @Field("lastName") String lName,
                                                 @Field("email") String email,
                                                 @Field("countryCode") String countryCode,
                                                 @Field("mobile") String mobileNumber,
                                                 @Field("deviceType") int device);

    @FormUrlEncoded
    @POST("/signIn")
    Observable<Response<ResponseBody>> signIn(@Header("lan") String selLang,
                                              @Field("mobileOrEmail") String mobileEmail,
                                              @Field("countryCode") String countryCode,
                                              @Field("deviceType") int device);

    @GET("/stream/live")
    Observable<Response<ResponseBody>> getLiveStreamers(@Header("authorization") String auth,
                                                        @Header("lan") String selLang);
    @FormUrlEncoded
    @POST ("/stream/event")
    Observable<Response<ResponseBody>> postApiCallStream(@Header("authorization") String auth,
                                                         @Header("lan") String selLang,
                                                         @Field("id") String streamId,
                                                         @Field("type") String streamType,
                                                         @Field("streamName") String streamName,
                                                         @Field("thumbnail") String thumbNail,
                                                         @Field("record") boolean record,
                                                         @Field("detection") boolean detection);
    @FormUrlEncoded
    @POST ("/stream/chat/{streamId}")
    Observable<Response<ResponseBody>> postApiChatStream(@Header("authorization") String auth,
                                                         @Header("lan") String selLang,
                                                         @Path("streamId") String streamId,
                                                         @Field("message") String msg);
    @GET ("/stream/chat/{streamId}/{timestamp}")
    Observable<Response<ResponseBody>> getApiChat(@Header("authorization") String auth,
                                                  @Header("lan") String selLang,
                                                  @Path("streamId") String streamId,
                                                  @Path("timestamp") long timestamp);
    @FormUrlEncoded
    @POST ("/stream/subscribe")
    Observable<Response<ResponseBody>> postApiSubscribe(@Header("authorization") String auth,
                                                        @Header("lan") String selLang,
                                                        @Field("id") String streamId,
                                                        @Field("action") String msg);
}
