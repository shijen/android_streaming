package com.livestreaming.newlivestream.mainHome;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.livestreaming.newlivestream.R;
import com.livestreaming.newlivestream.liveVideoPlayerStreamsLive.liveVideoBroadcaster.LiveVideoBroadcasterActivity;
import com.livestreaming.newlivestream.utility.AppPermissionsRunTime;
import com.livestreaming.newlivestream.utility.UploadAmazonS3;
import com.livestreaming.newlivestream.utility.VariableConstant;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

import static com.livestreaming.newlivestream.utility.VariableConstant.AmazonThumb;

public class PreviewActivity extends DaggerAppCompatActivity implements PreviewInterFace.PreviewView {

    private final int REQUEST_CODE_PERMISSION_MULTIPLE = 11;
    private String streamName = "rtmpstream-";
    private static final String IMAGE_DIRECTORY = "/YourDirectName";
    private String streamType;
    private String streamId = "";
    private String thumbnail = "";
    private String TAG = PreviewActivity.class.getSimpleName();
    private File wallpaperDirectory,fImage;
    UploadAmazonS3 upload;
    @Inject PreviewInterFace.PreViewPresenter preViewPresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);
        upload = UploadAmazonS3.getInstance(this, VariableConstant.Amazoncognitoid);
        checkForPermission();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    /**
     * <h2>checkPermission</h2>
     * <p>checking for the permission for camera and file storage at run time for
     * build version more than 22
     */
    private void checkForPermission()
    {
        if (Build.VERSION.SDK_INT >= 23) {
            ArrayList<AppPermissionsRunTime.MyPermissionConstants> myPermissionConstantsArrayList = new ArrayList<>();
            myPermissionConstantsArrayList.clear();
            myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_CAMERA);
            myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_WRITE_EXTERNAL_STORAGE);
            myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_READ_EXTERNAL_STORAGE);

            if (AppPermissionsRunTime.checkPermission(this, myPermissionConstantsArrayList, REQUEST_CODE_PERMISSION_MULTIPLE))
                selectImage();
        }else
            selectImage();
    }

    /**
     * predefined method to check run time permissions list call back
     * @param requestCode   request code
     * @param permissions:  contains the list of requested permissions
     * @param grantResults: contains granted and un granted permissions result list
     */
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean isDenine = false;
        switch (requestCode) {
            case REQUEST_CODE_PERMISSION_MULTIPLE:
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        isDenine = true;
                    }
                }
                if (isDenine) {
                    Toast.makeText(this,getResources().getString(R.string.app_doesnot_work_without_FilePermisssion)+ " " + REQUEST_CODE_PERMISSION_MULTIPLE,Toast.LENGTH_SHORT).show();
                } else {
                    selectImage();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }


    private void selectImage() {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK && intent.hasExtra("data")) {
                    Bitmap bitmap = (Bitmap) intent.getExtras().get("data");

                    if (bitmap != null) {
                        streamName = streamName + System.currentTimeMillis();
                         thumbnail = saveImage(bitmap);

                        //  ivThumbnailPhoto.setImageBitmap(bitmap);
                        streamType = "start";
                        File fime = new File(thumbnail);
                        Log.d(TAG, "onActivityResult: "+thumbnail+" FILENAME "+fime.getName());

                        thumbnail = "//s3-ap-southeast-1.amazonaws.com/appscrip/"+AmazonThumb+"/"+fime.getName();
                        preViewPresenter.onLiveBroadCastApi(streamId,streamType,thumbnail,streamName);

                        upload.Upload_data(VariableConstant.Amazonbucket + "/" + AmazonThumb,fime, new UploadAmazonS3.Upload_CallBack() {
                            @Override
                            public void sucess(String sucess) {
                                Log.d("TAG", "onSuccessAdded: " + sucess);
                              //  preViewPresenter.onLiveBroadCastApi(streamId,streamType,sucess,streamName);
                              //  profilePicUrl = image;
                                wallpaperDirectory.delete();
                                fImage.delete();
                            }

                            @Override
                            public void error(String errormsg) {
                                Log.d("TAG", "onerror: " + errormsg);
                            }
                        });



                    }

                }
                break;
                default:
                    checkForPermission();
                    break;
        }
    }

    private String saveImage(Bitmap bitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        wallpaperDirectory = new File(Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        if (!wallpaperDirectory.exists()) {  // have the object build the directory structure, if needed.
            wallpaperDirectory.mkdirs();
        }

        try {
            fImage = new File(wallpaperDirectory, Calendar.getInstance().getTimeInMillis() + ".jpg");
            fImage.createNewFile();
            FileOutputStream fo = new FileOutputStream(fImage);

            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{fImage.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::---&gt;" + fImage.getAbsolutePath());

            return fImage.getPath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    @Override
    public void onSuccess(String streamId) {
        Intent intent = new Intent(this, LiveVideoBroadcasterActivity.class);
        intent.putExtra("StreamName",streamName);
        intent.putExtra("StreamType",streamType);
        intent.putExtra("StreamId",streamId);
        intent.putExtra("ThumbNail",thumbnail);
        startActivity(intent);
        finish();
    }

    @Override
    public void onError(String error) {

    }
}
