package com.livestreaming.newlivestream.liveVideoPlayerStreamsLive.liveVideoPlayer;

import com.livestreaming.newlivestream.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * Created by Ali on 12/19/2018.
 */
@Module
public interface LiveVideoPlayerModule
{


    @Binds
    @ActivityScoped
    LiveVideoPresenterImpl.LivePresenter providePresenter(LiveVideoPlayerPresenter liveVideoPlayerPresenter);

    @Binds
    @ActivityScoped
    LiveVideoPresenterImpl.LiveView provideView(LiveVideoPlayer liveVideoPlayerPresenter);
}
